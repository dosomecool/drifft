package application.prototypes.custom.labeledbutton;

import java.io.IOException;
//import application.prototypes.DiffusionGraphPanel.Pathable;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class LabeledButton extends VBox{
	
	@FXML
	private Label lbTitleLabel, lbLabel;
	@FXML 
	private Button lbButton;
	
	private String componentName;
	// INITIALIZATION ACTION
	private Action action = Action.PLOT;
	
	public LabeledButton(String xComponentName)
	{
		// Constructor body
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource("/application/prototypes/custom/labeledbutton/LabeledButton.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.getStylesheets().add(getClass().getResource("/application/prototypes/custom/labeledbutton/LabeledButtonStyle.css").toExternalForm());
		
		this.componentName = xComponentName;
		lbTitleLabel.setText(componentName);
	}

	// THESE METHOD MUST BE OVERRIDEN IN THE CONSTRUCTOR
	public void plot() {
	}
	// THESE METHOD MUST BE OVERRIDEN IN THE CONSTRUCTOR
	public void cancel() {
	}
	// THESE METHOD MUST BE OVERRIDEN IN THE CONSTRUCTOR
	public void show() {
	}
	// THESE METHOD MUST BE OVERRIDEN IN THE CONSTRUCTOR
	public void hide() {
	}
	
	public enum Action{
		
		PLOT{
			@Override
			public void call(LabeledButton xLabeledButton){
				xLabeledButton.getButton().setText("PLOT");
				xLabeledButton.getButton().setOnAction(e -> {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							xLabeledButton.plot();
							Action.CANCEL.call(xLabeledButton);
						}
					});
				});
			}
		}, 
		CANCEL{
			@Override
			public void call(LabeledButton xLabeledButton){
				xLabeledButton.getButton().setText("CANCEL");
				xLabeledButton.getButton().setOnAction(e -> {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							xLabeledButton.cancel();
							// IF SUCCESS -> HIDE, IF FAIL -> PLOT
						}
					});
				});
			}
		}, 
		HIDE{
			@Override
			public void call(LabeledButton xLabeledButton){
				xLabeledButton.getButton().setText("HIDE");
				xLabeledButton.getButton().setOnAction(e -> {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							xLabeledButton.hide();
							Action.SHOW.call(xLabeledButton);
						}
					});
				});
			}
		}, 
		SHOW{
			@Override
			public void call(LabeledButton xLabeledButton){
				xLabeledButton.getButton().setText("SHOW");
				xLabeledButton.getButton().setOnAction(e -> {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							xLabeledButton.show();
							Action.HIDE.call(xLabeledButton);
						}
					});
				});
			}
		};
		
		public abstract void call(LabeledButton xLabeledButton);
	}
	
	@FXML
	public void initialize(){
		
		// ENTRY STATE OF THE SWITCH
		Action.PLOT.call(this);
	}
	
	protected Button getButton(){
		return lbButton;
	}
	
	public Label getLabel(){ 
		return lbLabel;
	}
	
	public void setAction(Action xAction){
		xAction.call(this);
	}
}
