package application.prototypes.custom;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

public class ProcessingPanel extends Pane {

	@FXML
	private Pane InProcessPane;
	
	public ProcessingPanel() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource("/application/prototypes/custom/Processing.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO; Add Stylesheet
//		this.getStylesheets().addAll(getClass()
//				.getResource(Constants.CSS_RESOURCE_PATH + "SceneStyle.css").toExternalForm());
		
	}
		
	public Pane getInProcessPane(){
		return InProcessPane;
	}
}
