package application.prototypes.custom.help;

import java.io.IOException;

import application.prototypes.main.MainController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Window;

public class Help extends Pane{

	private Window owner;
	private int index;
	Alert helpAlert;
	
	@FXML
	private ImageView helpView;

	Image[] album = new Image[]{
			new Image("1.0.png", 1000, 0, true, true),
			new Image("6.1.png", 1000, 0, true, true),
			new Image("6.2.png", 1000, 0, true, true),
			new Image("6.3.png", 1000, 0, true, true)
	};

	public Help(int index)
	{
		// Constructor body
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource("/application/prototypes/custom/help/Help.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.index = index;
		owner = MainController.getInstance().getScene().getWindow();	
		// this.getStylesheets().add(getClass().getResource("/application/prototypes/custom/help/HelpStyle.css").toExternalForm());
		lunchAlert();
	}
    	
	public void lunchAlert(){
		helpAlert = new Alert(AlertType.CONFIRMATION);
		helpAlert.setTitle("DRIFFT");
		helpAlert.setHeaderText("PAGE " + (index+1) + "/" + album.length);
		helpAlert.setContentText("Content Text");
		helpAlert.initOwner(owner);
		helpAlert.setResizable(false);
		helpAlert.getDialogPane().getStyleClass().addAll(".alert");
		
		helpView.setImage(album[index]);
		
		final ButtonType prevButtonData = new ButtonType("PREVIOUS", ButtonData.BACK_PREVIOUS);
		final ButtonType nextButtonData = new ButtonType("NEXT", ButtonData.NEXT_FORWARD);
		final ButtonType cancelButtonData = new ButtonType("CLOSE", ButtonData.CANCEL_CLOSE);
		
		helpAlert.getButtonTypes().setAll(prevButtonData, nextButtonData, cancelButtonData);

		final Button prevButton = (Button) helpAlert.getDialogPane().lookupButton(prevButtonData);
		final Button nextButton = (Button) helpAlert.getDialogPane().lookupButton(nextButtonData);
		
		prevButton.addEventFilter(ActionEvent.ACTION, event -> {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					if(index != 0 && album[index-1] != null){
						index = index-1;
						helpView.setImage(album[index]);
						helpAlert.setHeaderText("PAGE " + (index+1) + "/" + album.length);
					} 
				}
			});
			event.consume();
		});
		
		nextButton.addEventFilter(ActionEvent.ACTION, event -> {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					if(index < album.length-1 && album[index+1] != null){
						index = index + 1;
						helpView.setImage(album[index]); 	
						helpAlert.setHeaderText("PAGE " + (index+1) + "/" + album.length);
					}
				}
			});
			event.consume();
		});
		
		helpAlert.getDialogPane().setContent(this);
		helpAlert.showAndWait();
	}
}
