package application.prototypes.conditioneddiffusion;

import java.util.concurrent.Callable;

import application.prototypes.main.Configuration;
import net.openhft.compiler.CompilerUtils;

public class ConditionedDiffusion implements Runnable {
	
	// ******************** USER INPUTRS FROM CONFIGURATION ********************

	private Configuration config;

	// x0, numPath, tmin, tmax, h, drift, diffusion, kill

	private String x_0;			         // Initial state
	private String num_paths;            // Number of Paths
	private String t_min;                // Min time
	private String t_max;                // Max time
	private String _h;                   // dt

	private String _mu;                  // Drift form
	private String _sigma;               // Diffusion form
	private String _kill;                // Killing form
	
	private String _seed;                // Random Seed

	// DEFAULT CONSTRUCTOR
	public ConditionedDiffusion(){

		config = Configuration.getInstance();

		getValues();                                   // Get values from properties file
		setConditionedDiffusionJavaCode();
		loadDynamicConditionedDiffusionClass();
		getDynamicConditionedDiffusionArrays();
	}

	public void getValues(){

		x_0       = config.getConfig("x0",           "value");
		num_paths = config.getConfig("number_paths", "value");
		t_min     = config.getConfig("limits",       "tmin");
		t_max     = config.getConfig("limits",       "tmax");
		_h        = config.getConfig("delta_time",   "value");
		_mu       = config.getConfig("drift",        "mu");
		_sigma    = config.getConfig("diffusion",    "sigma");
		_kill     = config.getConfig("kill",         "function");
		_seed     = config.getConfig("seed",         "value");
	}
	
	// ******************** DYNAMIC DIFFUSION CLASS ********************

	private Object[] conditionedDiffusionArray;     // Object from Dynamic Class

	private String conditionedDiffusionJavaCode;    // String contains code for Dynamic Class

	// DIFFUSION STRING CLASS BEGINS 
	String dynamicConditionedDiffusionClass = "application.prototypes.conditioneddiffusion.DynamicConditionedDiffusionClass";

	public void setConditionedDiffusionJavaCode(){

		conditionedDiffusionJavaCode =  new String( "package application.prototypes.conditioneddiffusion; \n\n"

									 + "import application.prototypes.main.MainController; \n"
									 + "import javafx.concurrent.Task; \n"
									 + "import javafx.fxml.FXML; \n"
									 + "import static java.lang.Math.*; \n"
					 		  		 + "import java.util.concurrent.Callable; \n"
						 		     + "import java.util.Random; \n\n"
	
				    	 		     + "public final class DynamicConditionedDiffusionClass implements Callable<Object[]>, Runnable { \n\n"
	
				    	 		     // Main Controller
				    	 		     + "private MainController mainPane = MainController.getInstance(); \n\n"
				    	 		     
				    	 		     + "private Thread cdThread; \n\n"
				    	 		     
				    	 		     // x0
				    	 		     + "private double x0; \n"
	
				    	 		     // [0] - numPath
				    	 		     + "private int numPaths; \n\n"
	
				    	 		     // t
				    	 		     + "private double t; \n"  
				    	 		     
				    	 		     // x
				    	 		     + "private double x; \n" 
				    	 		     
				    	 		     // logdx
				    	 		     + "private double logdx; \n"
	
				    	 		     // tmin
				    	 		     + "private double tmin; \n"
				    	 		     
				    	 		     // tmax
				    	 		     + "private double tmax; \n"
	
				    	 		     // [1] - h
				    	 		     + "private double h; \n"                   // dt
	
				    	 		     // [2] - m
				    	 		     + "private int m; \n"                      // temporal interval
	
				    	 		     // Random Numbers
				    	 		     + "private double[][] dw; \n"
				    	 		     
				    	 		     // [3] - time
				    	 		     + "private double[] time; \n"              // time for each (t)
	
				    	 		     // [4] - xStar
				    	 		     + "private double[][] xStar; \n\n"
	
				    	 		     + "private long seed; \n\n"                // random seed
	
				    	 		     // Returned Values: x0, numPath, tmin, tmax, h, m, time, xState, kill
	
				    	 		     // BEGIN CONSTRUCTOR
				    	 		     + "public DynamicConditionedDiffusionClass() \n"
				    	 		     + "{ \n"
	
						 	  	     // INSERTED VALUE - *** x0 ***
						 	  	     + "x0 = " + x_0 + "; \n"
	
						 	  	     // INSERTED VALUE - *** num_paths ***
						 	  	     + "numPaths = " + num_paths + "; \n\n"
	
						 	  	     // INSERTED VALUE - *** t_min, t_max ***
						 	  	     + "tmin = " + t_min + "; \n"
						 	  	     + "tmax = " + t_max + "; \n"
	
						 	  	     // INSERTED VALUE - *** h ***
						 	  	     + "h = " + _h + "; \n\n"
	
						 	  	     // INSERTED VALUE - *** _seed ***
				 	  	             + "seed = " + _seed + "L; \n\n"
						 	  	     
						 	  	     // CALCULATES *** m *** BY USING *** tempM *** INTEGER VALUE
						 	  	     + "Double tempM = new Double( ( (tmax-tmin) / h) ); \n"
						 	  	     + "m = tempM.intValue(); \n\n"
						 	  	     
						 	  	     // RANDOM NUMBERS
                                     + "dw = new double[numPaths][m+1]; \n"
						 		  	 
						 		  	 // TIME
						 		  	 + "time = new double[m+1]; \n\n"
	
						 		  	 // STATE X STAR
						 		  	 + "xStar = new double[numPaths][m+1]; \n\n"
	
						 		  	 // CALL METHODS HERE
						 		  	 + "setDw(); \n"
						 		  	 + "setTime(); \n"
						 		  	 + "setTaskListener(); \n\n"
						 		  	 
						 		  	 + "cdThread = new Thread(genxStarTask); \n"
						 		  	 + "cdThread.setDaemon(true);\n"
						 		  	 + "cdThread.start(); \n\n"
	
						 		  	 + "} \n\n" 
						 		  	 // END CONSTRUCTOR
	
						 		  	 // TODO: Seed
					     		  	 + "Random dW = new Random(seed); \n\n"
	
					     		  	 // setdW() - RANDOM NUMBERS
					     		  	 + "	public void setDw() \n"
					     		  	 + "{ \n" 
					     		  	 
					     		  	 + "for(int index = 0; index < numPaths; index++) \n"
					     		  	 + "{ \n"
					     		  	 
					     		  	 + "for(int j=0; j<=m; j++) \n"
					     		  	 + "{ \n"
					     		  	 
					     		  	 + "dw[index][j] = dW.nextGaussian(); \n"
					     		  	 
		   							 + "} \n"
      								 + "} \n"
									 + "} \n\n"
					     		  	
					     		  	// setTime() - TIME INDEX
					     		  	+ "public void setTime() \n"
					     		  	+ "{ \n"
	
						 	      	+ "for(int j=0; j<= m; j++) \n"
						 	      	+ "{ \n"
						 	      	+ "time[j] = tmin + j * h; \n"
	
						 		  	+ "} \n"
						 		  	+ "} \n\n"
	
						 		    // TASK - RUNS THE LOOPS IN A NEW THREAD
						 		  	+ "private Task genxStarTask = new Task<Void>() { \n"
						 		  	+ "@Override public Void call() { \n"
						 		  	        
						 		  	+ "for(int index = 0; index < numPaths; index++) \n"
									+ "{ \n"
						 		  	
									//	Use >>> updateProgress(i, max) <<< as an alternate method to update progress bar. 
									+ "mainPane.getProgressBar().setProgress( ( (index+1) / (double) numPaths ) ); \n"
												  	
									// INITIAL STATE
									+ "xStar[index][0] = x0; \n"
					
									+ "for(int j=0; j<= m-1 ; j++) \n"
									+ "{ \n"
					
									+ "t = time[j]; \n"
									+ "x = xStar[index][j]; \n"
									+ "logdx = getlogdx(x, j); \n"
									
									// INSERTED VALUE - *** mu, sigma ***
									+ "xStar[index][j+1] = x + ( " + _mu + " + " + _sigma + " * logdx ) * h + " + _sigma + " * Math.sqrt(h) * dw[index][j]; \n"
					
									+ "if (isCancelled()) { \n"
									+ "mainPane.getProgressBar().setProgress( 0.0 ); \n"
                                    + "break; \n"
                                    + "} \n\n"
									
									+ "} \n"
									+ "} \n"
						 		  	    	
						 		  	+ "return null; \n"
						 		  	+ "} \n"
						 		  	+ "}; \n\n" // END OF TASK
	
						 		  	// getlogdx(double xValue, int tIndex)- Calculates the Log Derivative of Survival Mean 
						 		  	+ "public double getlogdx(double xValue, int tIndex){ \n"
	
								  	+ "double _x1; \n"
								  	+ "double _x2; \n\n"
	
								  	+ "double _kill1; \n"
								  	+ "double _kill2; \n\n"
	
								  	+ "double _survSum1 = 0; \n"
								  	+ "double _survSum2 = 0; \n\n"
	
								  	+ "double _survival1; \n"
								  	+ "double _survival2; \n\n"
	
							      	+ "double _survMean1 = 0; \n"
							      	+ "double _survMean2 = 0; \n\n"
	
							      	+ "double _dxLog = 0; \n\n" 
	
						 		  	+ "for(int index = 0; index < numPaths; index++) \n"
						 		  	+ "{ \n"
	
								  	+ "_x1 = xValue + 0.001; \n"
								  	+ "_x2 = xValue - 0.001; \n\n"
	
								  	+ "_kill1 = 0; \n"
								  	+ "_kill2 = 0; \n\n"
	
								  	+ "_survival1 = 1; \n"
								  	+ "_survival2 = 1; \n\n"
	
								  	+ "for(int j=0; j<= tIndex; j++) \n"
								  	+ "{ \n"
	
								  	+ "_x1 = _x1 + " + _mu.replace("x","_x1") + " * h + " + _sigma.replace("x","_x1") + " * Math.sqrt(h) * dw[index][j]; \n"  
								  	+ "_x2 = _x2 + " + _mu.replace("x","_x2") + " * h + " + _sigma.replace("x","_x2") + " * Math.sqrt(h) * dw[index][j]; \n\n"
	
								  	+ "_kill1 = " + _kill.replace("x","_x1") + "; \n"
								  	+ "_kill2 = " + _kill.replace("x","_x2") + "; \n\n"
	
								  	+ "_survival1 = (1 - _kill1 * h) * _survival1; \n"
								  	+ "_survival2 = (1 - _kill2 * h) * _survival2; \n\n"
	
								  	+ "} \n" // End of First Loop
	
								  	+ "_survSum1 = _survSum1 + _survival1; \n"
								  	+ "_survSum2 = _survSum2 + _survival2; \n\n"
	
								  	+ "} \n" // End of Second Loop
	
								  	+ "_survMean1 = _survSum1 / numPaths; \n"
								  	+ "_survMean2 = _survSum2 / numPaths; \n\n"
	
								  	+ "_dxLog = ( ( _survMean1 - _survMean2 ) / 0.002 ) / ( ( _survMean1 + _survMean2 ) / 2 ); \n"
	
								  	+ "return _dxLog; \n"
								  	+ "} \n\n"
	
								  	+ "@Override \n"
								  	+ "public Object[] call() throws Exception { \n"
								  	
								  	// numPath, h, m, time, xStar
								  	+ "return new Object[]{numPaths, h, m, time, xStar}; \n"
								  	+ "} \n\n"
	
								  	// Runnable
									+ "@Override \n"
									+ "public void run(){ \n"
	
									// Cancel Task
									+ "genxStarTask.cancel(); \n"
									+ "} \n\n"
								  	
								  	+ "public void setTaskListener(){ \n"

								  	+ "genxStarTask.setOnSucceeded(e ->{ \n"
								  	
								  	+ "mainPane.getProgressBar().setProgress( ( (0) / (double) numPaths ) ); \n"
								  	+ "mainPane.stepConditionedDiffusionProcess();"
									
								  	+ "}); \n\n"
									
									+ "} \n"
								  
								  	+ "} \n\n" );
		
		// System.out.print(conditionedDiffusionJavaCode);

	} // STRING CLASS ENDS

	@SuppressWarnings("rawtypes")
	private Class conditionedDiffusionClass;
	private Callable<Object[]> callConditionedDiffusion;
	private ClassLoader conditionedDiffusionClassloader;
	private Runnable runConditionedDiffusion;
	
	@SuppressWarnings("unchecked")
	public void loadDynamicConditionedDiffusionClass(){

		conditionedDiffusionClassloader = new ClassLoader() {
		};

		try {
			conditionedDiffusionClass = CompilerUtils.CACHED_COMPILER.loadFromJava(
					conditionedDiffusionClassloader, dynamicConditionedDiffusionClass, conditionedDiffusionJavaCode);
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			callConditionedDiffusion = (Callable<Object[]>) conditionedDiffusionClass.newInstance();
			runConditionedDiffusion = (Runnable) callConditionedDiffusion;
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void cancelTask(){
		
		try {
			runConditionedDiffusion.run();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getDynamicConditionedDiffusionArrays(){
		try {
			// obj = (Object[]) runner.getClass().newInstance().call();

			conditionedDiffusionArray = (Object[]) callConditionedDiffusion.call();

			// http://stackoverflow.com/questions/6337075/return-two-arrays-in-a-method-in-java

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		};
	}

	// DYNAMIC DIFFUSION GETTERS // numPath, h, m, time, xStar

	public int getNumPaths(){
		return (int) conditionedDiffusionArray[0];
	}

	public double getH(){
		return (double) conditionedDiffusionArray[1];
	}

	public int getM(){
		return (int) conditionedDiffusionArray[2];
	}

	public double[] getTime(){
		return (double[]) conditionedDiffusionArray[3];
	}

	public double[][] getxStar(){
		return (double[][]) conditionedDiffusionArray[4]; 
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}    
}