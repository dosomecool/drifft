package application.prototypes.killing;

import java.io.IOException;

import application.prototypes.custom.labeledbutton.LabeledButton;
import application.prototypes.custom.labeledbutton.LabeledButton.Action;
import application.prototypes.main.Constants;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class DiffusionKillingGraphPanel extends AnchorPane {

	@FXML
	private AnchorPane diffusionSidePanel;
	
	private static DiffusionKillingGraph controller;
		
	private static Thread pathThread;	
	private static Thread varianceAreaThread;
	private static Thread varianceLineThread;
	private static Thread meanThread;
	
	public DiffusionKillingGraphPanel(DiffusionKillingGraph xController) 
	{
		// MISSLABELED: THIS IS THE DIFFUSION GRAPH WHERE GRAPHS ARE CREATED
		controller = xController;
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource(Constants.FXML_RESOURCE_PATH + "diffusionGraphPanel.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		diffusionSidePanel.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "ItoGraphStyle.css").toExternalForm());
	}
	
	private static LabeledButton pathSwitch = new LabeledButton("PATH"){
		@Override
		public void plot(){
			pathSwitch.getLabel().textProperty().bind(controller.getPathDataTask().messageProperty());
			pathThread = new Thread(controller.getPathDataTask());
			pathThread.setDaemon(true);
			pathThread.start();
			controller.getPathDataTask().setOnSucceeded(e ->{
				pathSwitch.setAction(Action.HIDE);
			});
		}
		@Override
		public void cancel(){
			if (controller.getPathDataTask().isRunning()){
				controller.getPathDataTask().cancel();
			}
		}
		@Override
		public void show(){
			if (controller.getPathDataTask().isDone()){
				controller.getPathChart().getData().addAll(DiffusionKillingGraph.getPathSeries());
			}
		}
		@Override
		public void hide(){
			controller.getPathChart().getData().remove(0, controller.getNumPath());
		}
	};
	
	private static LabeledButton meanSwitch = new LabeledButton("MEAN"){
		@Override
		public void plot(){
			meanSwitch.getLabel().textProperty().bind(controller.getMeanDataTask().messageProperty());
			meanThread = new Thread(controller.getMeanDataTask());
			meanThread.setDaemon(true);
			meanThread.start();
			controller.getMeanDataTask().setOnSucceeded(e ->{
				meanSwitch.setAction(Action.HIDE);
			});
		}
		@Override
		public void cancel(){
			// TODO
			
		}
		@Override
		public void show(){
			if (controller.getMeanDataTask().isDone()){
				controller.getPathMeanChart().getData().addAll(DiffusionKillingGraph.getMeanSeries());
			}
		}
		@Override
		public void hide(){
			controller.getPathMeanChart().getData().remove(0);
		}
	};
	
	private static LabeledButton varLineSwitch = new LabeledButton("SD LINE"){
		@Override
		public void plot(){
			varLineSwitch.getLabel().textProperty().bind(controller.getVarianceLineDataTask().messageProperty());
			varianceLineThread = new Thread(controller.getVarianceLineDataTask());
			varianceLineThread.setDaemon(true);
			varianceLineThread.start();
			controller.getVarianceLineDataTask().setOnSucceeded(e ->{
				varLineSwitch.setAction(Action.HIDE);
			});
		}
		@Override
		public void cancel(){
			// TODO
		}
		@Override
		public void show(){
			if (controller.getVarianceLineDataTask().isDone()){
				controller.getPathVarianceLineChart().getData().addAll(DiffusionKillingGraph.getVarianceLineSeries());
			}
		}
		@Override
		public void hide(){
			controller.getPathVarianceLineChart().getData().remove(0,2);
		}
	};
	
	private static LabeledButton varAreaSwitch = new LabeledButton("SD AREA"){
		@Override
		public void plot(){
			varAreaSwitch.getLabel().textProperty().bind(controller.getVarianceAreaDataTask().messageProperty());
			varianceAreaThread = new Thread(controller.getVarianceAreaDataTask());
			varianceAreaThread.setDaemon(true);
			varianceAreaThread.start();
			controller.getVarianceAreaDataTask().setOnSucceeded(e ->{
				varAreaSwitch.setAction(Action.HIDE);
			});
		}
		@Override
		public void cancel(){
			// TODO
		}
		@Override
		public void show(){
			if (controller.getVarianceAreaDataTask().isDone()){
				controller.getPathVarianceAreaChart().getData().addAll(DiffusionKillingGraph.getVarianceAreaSeries());
			}
		}
		@Override
		public void hide(){
			controller.getPathVarianceAreaChart().getData().remove(0,2);
		}
	};
		
	@FXML
	public void initialize(){
		
		pathSwitch.getLabel().textProperty().unbind();
		pathSwitch.getLabel().setText("PENDING");
		meanSwitch.getLabel().textProperty().unbind();
		meanSwitch.getLabel().setText("PENDING");
		varLineSwitch.getLabel().textProperty().unbind();
		varLineSwitch.getLabel().setText("PENDING");
		varAreaSwitch.getLabel().textProperty().unbind();
		varAreaSwitch.getLabel().setText("PENDING");
		
		pathSwitch.setAction(Action.PLOT);
		meanSwitch.setAction(Action.PLOT);
		varLineSwitch.setAction(Action.PLOT);
		varAreaSwitch.setAction(Action.PLOT);
		
		AnchorPane.setLeftAnchor(pathSwitch, (double) 25);
		AnchorPane.setLeftAnchor(meanSwitch, (double) 25);
		AnchorPane.setLeftAnchor(varLineSwitch, (double) 25);
		AnchorPane.setLeftAnchor(varAreaSwitch, (double) 25);
		
		diffusionSidePanel.getChildren().add(pathSwitch);
		pathSwitch.setLayoutY(30);
		
		diffusionSidePanel.getChildren().add(meanSwitch);
		meanSwitch.setLayoutY(90);
		
		diffusionSidePanel.getChildren().add(varLineSwitch);
		varLineSwitch.setLayoutY(150);
		
		diffusionSidePanel.getChildren().add(varAreaSwitch);
		varAreaSwitch.setLayoutY(210);
	}
}