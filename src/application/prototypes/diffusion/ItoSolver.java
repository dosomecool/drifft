package application.prototypes.diffusion;

import application.prototypes.conditioneddiffusion.ConditionedDiffusion;
import application.prototypes.main.Configuration;

public class ItoSolver {

	private Configuration config;
	
	private Diffusion dynamicDiffusion;
	private ConditionedDiffusion dynamicConditionedDiffusion;
	
	// numPaths, h, m, time, xState, killing, survival

	// * NUMBER OF PATHS
	private int numPath;
	
	// * DELTA T
	private double h;
	
	// * TEMPORAL INTERVALS
	private int m;
	
	// * TIME INDEX
	private double[] t;
	
	// * X 
	private double[][] x;
	private double[]   meanX;
	private double[]   varianceX;
	
	// X-STAR
	private double[][] xStar;
	private double[]   meanxStar;
	private double[]   variancexStar;

	// KILLING
	private double[][] killing;
	private double[]   meanKilling;
	private double[]   varianceKilling;
	
	// SURVIVAL
	private double[][] survival;
	private double[]   meanSurvival;
	private double[]   varianceSurvival;
	
	// DENSITY
	private double[]   density;
	
	// HAZARD
	private double[]   hazard;
	
	// LIMITS
	private double tmin;
	private double tmax;
	
	// FLAG
	private boolean initialzed = false;
	
	// CONSTRUCTOR
//	public ItoSolver() 
//	{	
//		
//	}

	public void getAttributes(){
		
		if(!initialzed){
			config = Configuration.getInstance();
			tmin = Double.parseDouble(config.getConfig("limits", "tmin"));
			tmax = Double.parseDouble(config.getConfig("limits", "tmax"));
			h    = Double.parseDouble(config.getConfig("delta_time", "value"));
			Double temporaryM = new Double( ( (tmax-tmin) / h) );
			m = temporaryM.intValue();
			numPath = Integer.parseInt(config.getConfig("number_paths", "value"));
			initialzed = true;
		}
	}

	// ******************** X STATE METHODS ********************
	
	public void meanX()
	{	
		for(int j=0; j<=m; j++)
		{
			double meanSum = 0;
			
			for(int index = 0; index < numPath; index++ )
			{
				meanSum = meanSum + x[index][j];
			}
			meanX[j] = meanSum / numPath;
		}
	}
	
	public void varianceX()
	{	
		for(int j=0; j<=m; j++)
		{
			double varianceSum = 0;
			
			for(int index = 0; index < numPath; index++ )
			{
				varianceSum = varianceSum + 
						(( x[index][j] - meanX[j] ) * ( x[index][j] - meanX[j] ));
			}
			
			varianceX[j] = Math.sqrt(varianceSum / numPath);
		}
	}
	
	// ******************** X-STAR STATE METHODS ********************

	public void meanxStar() {	
		
		for (int j=0; j<= m; j++)
		{
			double meanSum = 0;

			for (int index = 0; index < numPath; index++ )
			{
				meanSum = meanSum + xStar[index][j];
			}
			meanxStar[j] = meanSum / numPath;
		}
	}
	
	public void variancexStar()
	{	
		for (int j=0; j<=m; j++)
		{
			double varianceSum = 0;
			
			for (int index = 0; index < numPath; index++ )
			{
				varianceSum = varianceSum + 
						(( xStar[index][j] - meanxStar[j] ) * ( xStar[index][j] - meanxStar[j] ));
			}
			
			variancexStar[j] = Math.sqrt(varianceSum / numPath);
		}
	}
	
	// ********************* KILLING STATE METHODS ********************
	
	public void meanKilling()
	{	
		for (int j=0; j<=m; j++)
		{
			double meanSum = 0;
		
			for (int index = 0; index < numPath; index++)
			{
				meanSum = meanSum + killing[index][j];
			}
			meanKilling[j] = meanSum / numPath;
		}
	}
	
	public void varianceKilling()
	{
		for(int j=0; j<=m; j++)
		{
			double varianceSum = 0;
			
			for(int index = 0; index < numPath; index++ )
			{
				varianceSum = varianceSum + 
						(( killing[index][j] - meanKilling[j] ) * 
								( killing[index][j] - meanKilling[j] ));
			}
			varianceKilling[j] = Math.sqrt(varianceSum / numPath);
		}
	}
	
	// ******************** SURVIVAL STATE METHODS ********************
	
	public void meanSurvival()
	{	
		for (int j=0; j<=m; j++)
		{
			double meanSum = 0;
		
			for (int index = 0; index < numPath; index++)
			{
				meanSum = meanSum + survival[index][j];
			}
			meanSurvival[j] = meanSum / numPath;
		}
	}
	
	public void varianceSurvival()
	{
		for(int j=0; j<=m; j++)
		{
			double varianceSum = 0;
			
			for(int index = 0; index < numPath; index++ )
			{
				varianceSum = varianceSum + 
						(( survival[index][j] - meanSurvival[j] ) * 
								( survival[index][j] - meanSurvival[j] ));
			}
			varianceSurvival[j] = Math.sqrt(varianceSum / numPath);
		}
	}
	
	// ******************** DENSITY STATE METHODS ********************
	
	private double densityMax = 0;
	
	public void itoDensity() 
	{
		density[0] = 0;
		
		for(int j=1; j<=m-1; j++)
		{
			density[j] = ( meanSurvival[j-1] - meanSurvival[j+1]) / (2*h);
			
			// Stores the highest x value
			if (density[j] > densityMax){
				densityMax = density[j];
			}
		}
	}
	
	// ******************** HAZARD STATE METHODS **********************
	
	private double hazardMax = 0;
	
	public void itoHazard() 
	{
		hazard[0] = 0;
		
		for(int j=1; j<=m-1; j++)
		{
			hazard[j] = density[j] / meanSurvival[j];;
			
			// Stores the highest x value
			if (hazard[j] > hazardMax){
				hazardMax = hazard[j];
			}
		}
	}
		
	public double[] getLimits(double[][] xArray){
		
		double min = 0;
		double max = 0;
		double[] limits = new double[2];
		
		for(int index = 0; index < numPath; index++ )
		{			
			for(int j=0; j<=m; j++)
			{
				// Stores the lowest x value
				if (xArray[index][j] < min){
					min = xArray[index][j];
				}
				
				// Stores the highest x value
				if (xArray[index][j] > max){
					max = xArray[index][j];
				}
			}
		}
		
		limits[0] = min;
		limits[1] = max;
		
		return limits;
	}

	double[] itoCumulativeDist;
	public void itoCumulativeDistribution() 
	{
		itoCumulativeDist = new double[m+1];
		itoCumulativeDist[0] = 0;

		for(int j=0; j<=m-1; j++)
		{
			itoCumulativeDist[j+1] = itoCumulativeDist[j] + density[j+1] * h;
		}
	}
	
	/**
	 * Run all methods within this class in proper order.
	 */
	// Consider moving this to the constructor 
	public void runSimulation()
	{
		//TODO
	}
	
	// *************************************************************************
	
	public void setDiffusion(Diffusion xDiffusion){
		
		dynamicDiffusion = xDiffusion;
		
		getAttributes();
		
		// TIME INDEX CALLED TWICE _ I DONT LIKE THAT
		t = dynamicDiffusion.getTime();
				
		// X STATE
		x = dynamicDiffusion.getxState();
		meanX = new double[m+1];
		varianceX = new double[m+1];
				
		// KILLING STATE
		killing = dynamicDiffusion.getKilling();
		meanKilling = new double[m+1];
		varianceKilling = new double[m+1];
		
		// SURVIVAL STATE
		survival = dynamicDiffusion.getSurvival();
		meanSurvival = new double[m+1];
		varianceSurvival = new double[m+1];
		
		// DENSITY STATE
		density = new double[m+1];
		
		// HAZARD STATE
		hazard = new double[m+1];
		
        meanX();
        varianceX();
        
        meanKilling();    		
    	varianceKilling();

        meanSurvival();
        varianceSurvival();
       
        itoDensity();
        itoHazard();
	}

	// *************************************************************************
	
	public void setConditionedDiffusion(ConditionedDiffusion xConditionedDiffusion){
		
		dynamicConditionedDiffusion = xConditionedDiffusion;
		
		getAttributes();
		
		// TIME INDEX CALLED TWICE _ I DONT LIKE THAT
		t = dynamicConditionedDiffusion.getTime();
		
		// X-STAR STATE
		xStar = dynamicConditionedDiffusion.getxStar();
		meanxStar = new double[m+1];
		variancexStar = new double[m+1];
		
		meanxStar();
		variancexStar();
	}
	
	// KILLING
	public double[][] getKilling() {
		return killing;
	}
	
	public double[] getMeanKilling() {
		return meanKilling;
	}
		
	public double[] getVarianceKilling() {
		return varianceKilling;
	}
	
	public double getKillMin(){
		return getLimits(killing)[0];
	}
	
	public double getKillMax(){
		return getLimits(killing)[1];
	}
	
	// TEMPORAL INTERVALS
	public int getM() {
		return m;
	}
	
	public int getNumPath() {
		return numPath;
	}

	public double gettMin() {
		return tmin;
	}
	
	public double gettMax() {
		return tmax;
	}
	
	public double[] getT(){
		return t;
	}
	
	// X
	public double[][] getX() {
		return x;
	}
	
	public double[] getMeanX() {
		return meanX;
	}
	
	public double[] getVarianceX() {
		return varianceX;
	}
	
	public double getxMin() {
		return getLimits(x)[0];
	}
	
	public double getxMax() {
		return getLimits(x)[1];
	}
	
	// X STAR
	public double[][] getxStar() {
		return xStar;
	}
	
	public double[] getMeanxStar() {
		return meanxStar;
	}
	
	public double[] getVariancexStar() {
		return variancexStar;
	}
	
	public double getxStarMin() {
		return getLimits(xStar)[0];
	}
	
	public double getxStarMax() {
		return getLimits(xStar)[1];
	}
	
	// SURVIVAL
	public double[][] getSurvival() {
		return survival;
	}
	
	public double[] getMeanSurvival() {
		return meanSurvival;
	}
		
	public double[] getVarianceSurvival() {
		return varianceSurvival;
	}

	// DENSITY
	public double[] getDensity() {
		return density;
	}
	
	public double getDensityMax() {
		return densityMax;
	}
	
	// HAZARD
	public double[] getHazard() {
		return hazard;
	}
	
	public double getHazardMax() {
		return hazardMax;
	}
	
	public double[] getItoCumulativeDist() {
		return itoCumulativeDist;
	}
	
	public double getItoLogCumulativeDist(int jIindex) {
		return Math.log(itoCumulativeDist[jIindex]);
	}
}
