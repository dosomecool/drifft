package application.prototypes.diffusion;

import java.util.concurrent.Callable;

import application.prototypes.main.Configuration;
import net.openhft.compiler.CompilerUtils;

public class Diffusion {
	
	// ******************** USER INPUTRS FROM CONFIGURATION ********************
	
	private Configuration config;
	
	// x0, numPath, tmin, tmax, h, drift, diffusion, kill
	
	private String x_0;			         // Initial state
	private String num_paths;            // Number of Paths
	private String t_min;                // Min time
	private String t_max;                // Max tima
	private String _h;                   // dt
	
	private String _mu;                  // Drift form
	private String _sigma;               // Diffusion form
	private String _kill;                // Killing form
	
	private String _seed;                // Seed

	// DEFAULT CONSTRUCTOR
	public Diffusion(){
		
		config = Configuration.getInstance();
		
		getValues();                                   // Get values from properties file
		setDiffusionJavaCode();
		loadDynamicDiffusionClass();
		getDynamicDiffusionArrays();
	}
	
	public void getValues(){
		
		x_0       = config.getConfig("x0",           "value");
		num_paths = config.getConfig("number_paths", "value");
		t_min     = config.getConfig("limits",       "tmin");
		t_max     = config.getConfig("limits",       "tmax");
		_h        = config.getConfig("delta_time",   "value");
		_mu       = config.getConfig("drift",        "mu");
		_sigma    = config.getConfig("diffusion",    "sigma");
		_kill     = config.getConfig("kill",         "function");
		_seed     = config.getConfig("seed",         "value");
	}
	
	// ******************** DYNAMIC DIFFUSION CLASS ********************
	
	private Object[] diffusionArray;     // Object from Dynamic Class
	
	private String diffusionJavaCode;    // String contains code for Dynamic Class
	
	// DIFFUSION STRING CLASS BEGINS 
	String dynamicDiffusionClass = "application.prototypes.diffusion.DynamicDiffusionClass";
	
	public void setDiffusionJavaCode(){
		
		diffusionJavaCode =  new String( "package application.prototypes.diffusion; \n\n"

                          + "import application.prototypes.main.MainController; \n"
                          + "import javafx.concurrent.Task; \n"
				 		  + "import java.util.concurrent.Callable; \n"
				 		  + "import static java.lang.Math.*; \n"
				 		  + "import java.util.Random; \n\n"

		    	 		  + "public final class DynamicDiffusionClass implements Callable<Object[]> { \n\n"

		    	 		  // Main Controller
		    	 		  + "private MainController mainPane = MainController.getInstance(); \n\n"
		    	 		  
		    	 		  + "private Thread dThread; \n\n"
		    	 		  
		    	 		  // x0
		    	 		  + "private double x0; \n"

		    	 		  // [0] - numPath
		    	 		  + "private int numPaths; \n\n"

		    	 		  // t
		    	 		  + "private double t; \n"  
		    	 		  
		    	 		  // x
		    	 		  + "private double x; \n"

		    	 		  // tmin
		    	 		  + "private double tmin; \n"
		    	 		  
		    	 		  // tmax
		    	 		  + "private double tmax; \n"

		    	 		  // [1] - h
		    	 		  + "private double h; \n"                   // dt

		    	 		  // [2] - m
		    	 		  + "private int m; \n"                      // temporal interval
		    	 		  
		    	 		  // RANDOM NUMBERS
		    	 		  + "private double[][] dw; \n\n"            // Wiener Process

		    	 		  // [3] - time
		    	 		  + "private double[] time; \n\n"            // time for each (t)

		    	 		  // [4] - xState
		    	 		  + "private double[][] xState; \n\n" 

		    	 		  // [5] - killing
		    	 		  + "private double[][] killing; \n\n"
		    	 		  
		    	 		  // [6] - survival
		    	 		  + "private double[][] survival; \n\n"

		    	 		  // TODO: SEED
		    	 		  + "private long seed; \n\n"
		    	 		  
		    	 		  // numPaths, h, m, time, xState, killing, survival

		    	 		  // BEGIN CONSTRUCTOR
		    	 		  + "public DynamicDiffusionClass() \n"
		    	 		  + "{ \n"

				 	  	  // INSERTED VALUE - *** x_0 ***
				 	  	  + "x0 = " + x_0 + "; \n"

				 	  	  // INSERTED VALUE - *** num_paths ***
				 	  	  + "numPaths = " + num_paths + "; \n\n"

				 	  	  // INSERTED VALUE - *** t_min, t_max ***
				 	  	  + "tmin = " + t_min + "; \n"
				 	  	  + "tmax = " + t_max + "; \n"

				 	  	  // INSERTED VALUE - *** _h ***
				 	  	  + "h = " + _h + "; \n\n"

				 	  	  // INSERTED VALUE - *** _seed ***
				 	  	  + "seed = " + _seed + "L; \n\n"
				 	  	  
				 	  	  // CALCULATES *** m *** BY USING *** tempM *** INTEGER VALUE
				 	  	  + "Double tempM = new Double( ( (tmax-tmin) / h) ); \n"
				 	  	  + "m = tempM.intValue(); \n\n"

				 	      // Random Numbers
				 		  + "dw = new double[numPaths][m+1]; \n"
				 		  
				 		  // TIME
				 		  + "time = new double[m+1]; \n"

				 		  // STATE X
				 		  + "xState = new double[numPaths][m+1]; \n"

				 		  // KILLING
				 		  + "killing = new double[numPaths][m+1]; \n\n"
				 		  
				 		  // SURVIVAL
				 		  + "survival = new double[numPaths][m+1]; \n\n"

				 		  // ************************************** Encapsula
				 		  
				 		  // CALL METHODS HERE
				 		  + "setDw(); \n"
				 		  + "setTime(); \n"

						  + "setTaskListener(); \n"
						  + "dThread = new Thread(genDiffusionTask); \n"
						  + "dThread.setDaemon(true); \n"
						  + "dThread.start(); \n\n"
				 		  
				 		  + "} \n\n"
				 		  // END CONSTRUCTOR

				 		  // RANDOM NUMBER GENERATOR
			     		  + "Random dW = new Random(seed); \n\n"

			     		  // setdW() - RANDOM NUMBERS
			     		  + "public void setDw() \n"
			     		  + "{ \n" 
			     		  
			     		  + "for(int index = 0; index < numPaths; index++) \n"
			     		  + "{ \n"
			     		  
			     		  + "for(int j=0; j<=m; j++) \n"
			     		  + "{ \n"
			     		  
			     		  + "dw[index][j] = dW.nextGaussian(); \n"
			     		  
						  + "} \n"
						  + "} \n"
						  + "} \n\n" // End of setdW();

			     		  // setTime() - TIME INDEX
			     		  + "public void setTime() \n"
			     		  + "{ \n"

				 	      + "for(int j=0; j<= m; j++) \n"
				 	      + "{ \n"
				 	      + "time[j] = tmin + j * h; \n"

				 		  + "} \n"
				 		  + "} \n\n" // End of setTime();
				 		  
				 		  // TASK - RUNS THE LOOPS IN A NEW THREAD
				 		  + "private Task genDiffusionTask = new Task<Void>() { \n"
				 		  + "@Override public Void call() { \n"
				 		  
				 		  + "genPaths(); \n"
				 		  + "genKilling(); \n"
				 		  + "genSurvival(); \n\n"
					 	  
					 	  + "return null; \n"
					 	  + "} \n"
					 	  + "}; \n\n" // END OF TASK
				 		  
				 		  // genPaths() - REGULAR DIFFUSION DEFINES X STATES
				 		  + "public void genPaths() \n"
				 		  + "{ \n"
				 		  
				 		  + "for(int index = 0; index < numPaths; index++) \n"
				 		  + "{ \n"
				 		  
				 		  // INITIAL STATE
				 		  + "xState[index][0] = x0; \n"

						  + "for(int j=0; j<= m-1 ; j++) \n"
				 		  + "{ \n"
				 		  
				 		  + "t = time[j]; \n"
				 		  + "x = xState[index][j]; \n\n"
				 		  
				 		  // INSERTED VALUE - *** mu, sigma ***
				 		  + "xState[index][j+1] = x + " + _mu + " * h + " + _sigma + " * Math.sqrt(h) * dw[index][j]; \n"

				 		  + "} \n"
				 		  + "} \n"
				 		  + "} \n\n"
						  
						  // genKilling() - GENERATES KILLING
						  + "public void genKilling() \n"
						  + "{ \n"
						  + "for(int index = 0; index < numPaths; index++) \n"
						  + "{ \n"
				  
						  + "for(int j=0; j<=m ; j++) \n"
						  + "{ \n"
		
						  + "t = time[j]; \n"
						  + "x = xState[index][j]; \n"
						  
						  // INSERTED VALUE - *** kill ***
						  + "killing[index][j] = " + _kill + "; \n"
						  
						  + "} \n" 	
						  + "} \n"
						  + "} \n\n"
						  
						  // genSurvival() - GENERATES SURVIVAL
						  + "public void genSurvival() \n"
						  + "{ \n"
						  + "for(int index = 0; index < numPaths; index++) \n"
						  + "{ \n"
						  
						  // INITIAL CONDITION
						  + "survival[index][0]=1; \n" 
						  
						  + "for(int j=0; j<= m-1 ; j++) \n"
						  + "{ \n"
		
						  + "t = time[j]; \n"
						  + "x = xState[index][j]; \n"
						  
						  // INSERTED VALUE - *** kill ***
						  + "survival[index][j+1] = (1 - " + _kill + " * h) * survival[index][j]; \n"
						  
						  + "} \n" 	
						  + "} \n"
						  + "} \n\n"

						  // ***********************************************************************************
						  
						  + "public void setTaskListener(){ \n"

						  + "genDiffusionTask.setOnSucceeded(e ->{ \n"
						  	
						  + "// mainPane.getProgressBar().setProgress( ( (0) / (double) numPaths ) ); \n"
						  + "mainPane.stepDiffusionProcess();"
							
						  + "}); \n\n"
							
						  + "} \n"
						  
						  // *************************************************************************************
						  
						  + "@Override \n"
						  + "public Object[] call() throws Exception { \n"
		
						  // numPaths, h, m, time, xState, killing, survival
						  + "return new Object[]{numPaths, h, m, time, xState, killing, survival}; \n"
						  + "} \n"
		
						  + "} \n\n" );
		
	// System.out.print(diffusionJavaCode);
		
	} // STRING CLASS ENDS
		
	@SuppressWarnings("rawtypes")
	private Class diffusionClass;
	private Callable<Object[]> callDiffusion;
	private ClassLoader diffusionClassloader;
	
	@SuppressWarnings("unchecked")
	public void loadDynamicDiffusionClass(){
		
		diffusionClassloader = new ClassLoader() {
		};
		
		try {
			diffusionClass = CompilerUtils.CACHED_COMPILER.loadFromJava(diffusionClassloader, dynamicDiffusionClass, diffusionJavaCode);
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			callDiffusion = (Callable<Object[]>) diffusionClass.newInstance();

		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    public void getDynamicDiffusionArrays(){
    	try {
//    		obj = (Object[]) runner.getClass().newInstance().call();
    		
    		diffusionArray = (Object[]) callDiffusion.call();
    		    		
    	// http://stackoverflow.com/questions/6337075/return-two-arrays-in-a-method-in-java
    		
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		};
    }
    
    // DYNAMIC DIFFUSION GETTERS // numPaths, h, m, time, xState, killing, survival
    
    public int getNumPaths(){
		return (int) diffusionArray[0];
    }
    
    public double getH(){
		return (double) diffusionArray[1];
    }
    
    public int getM(){
		return (int) diffusionArray[2];
    }
    
    public double[] getTime(){
 		return (double[]) diffusionArray[3];
    }
    
    public double[][] getxState(){
		return (double[][]) diffusionArray[4];
    }
    
    public double[][] getKilling(){
		return (double[][]) diffusionArray[5];
    }
    
    public double[][] getSurvival(){
		return (double[][]) diffusionArray[6];
    }
}
