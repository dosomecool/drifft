package application.prototypes.survivalpde;

import java.io.IOException;

import application.prototypes.custom.labeledbutton.LabeledButton;
import application.prototypes.custom.labeledbutton.LabeledButton.Action;
import application.prototypes.density.DiffusionDensityGraph;
import application.prototypes.main.Constants;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class FTCSGraphPanel extends AnchorPane {

	@FXML
	private AnchorPane diffusionSidePanel;
	
	private static FTCSGraph controller;

	private static Thread meanThread;
	
	// DiffusionDensityGraph
	
	public FTCSGraphPanel(FTCSGraph xController) 
	{
		controller = xController;
		
		// Constructor body
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource(Constants.FXML_RESOURCE_PATH + "diffusionGraphPanel.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		diffusionSidePanel.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "ItoGraphStyle.css").toExternalForm());
	}
			
	private static LabeledButton meanSwitch = new LabeledButton("SURVIVAL"){
		@Override
		public void plot(){
			meanSwitch.getLabel().textProperty().bind(controller.getPathDataTask().messageProperty());
			meanThread = new Thread(controller.getPathDataTask());
			meanThread.setDaemon(true);
			meanThread.start();
			controller.getPathDataTask().setOnSucceeded(e ->{
				meanSwitch.setAction(Action.HIDE);
			});
		}
		@Override
		public void cancel(){
			// TODO
			
		}
		@Override
		public void show(){
			if (controller.getPathDataTask().isDone()){
				controller.getPathChart().getData().addAll(DiffusionDensityGraph.getMeanSeries());
			}
		}
		@Override
		public void hide(){
			controller.getPathChart().getData().remove(0);
		}
	};
	
	@FXML
	public void initialize(){
		
		meanSwitch.getLabel().textProperty().unbind();
		meanSwitch.getLabel().setText("PENDING");
		
		meanSwitch.setAction(Action.PLOT);
		
		AnchorPane.setLeftAnchor(meanSwitch, (double) 25);
		
		diffusionSidePanel.getChildren().add(meanSwitch);
		meanSwitch.setLayoutY(30);
	}
}
