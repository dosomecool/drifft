package application.prototypes.survivalpde;

public class FTCSSolver {

	private FTCSParameters dp; 
	
	// SPATIAL INTERVALS
	private int n;
	// TEMPORAL INTERVALS
	private int m;
	
	//Delta (t)
	private double h;
	// Delta (x)
	private double k;
	
	// Stores the solution
	private double[][] solution;
	private double[][] points;
	
	private double[] x;
	private double[] t;
	
	private double tmin;
	private double tmax;
	
	private double xmin;
	private double xmax;
	
	public FTCSSolver()
	{	
		dp = new FTCSParameters(); 
				
		m = dp.getM();
		n = dp.getN();
		
		x = dp.getX();
		t = dp.getT();
		
		solution = new double[n+1][m+1];
		points = new double[n+1][m+1];
		
		tmin = dp.getMinT();
		tmax = dp.getMaxT();
		
		xmin = dp.getMinX();
		xmax = dp.getMaxX();
		
		h = dp.getH();
		k = dp.getK();
		
		initializeSolutionArray();
		setInitialCondition();
		setBoundaryCondition();
		solve();
		setPoints();
	}
	
	// Initialize array that holds solution
	public void initializeSolutionArray()
	{
		for(int j=0; j<=m; j++)
		{
			for(int i=0; i<=n; i++)
			{
				solution[i][j]=0;
			}
		}
	}
	
	// Set initial condition FBAR[x,0]=1;
	public void setInitialCondition()
	{
		for(int i=0; i<=n; i++)
		{
			solution[i][0]=1;
		}
	}
	
	// Set boundary condition
	public void setBoundaryCondition()
	{
		for(int j=0; j<=m-1; j++)
		{
			solution[0][j+1]=0;
			solution[n][j+1]=0;
		}
	}
	
	double mu;
	double sig;
	double kill;
	
	// Solve
	public void solve()
	{	
		// System.out.println("m: " + m);
		// System.out.println("n: " + n);
		
		for(int j=0; j<=m-1; j++)
		{
			for(int i=0; i<=n-2; i++) // n-2
			{
				mu   = dp.getDrift()[i+1][j];
				sig  = dp.getDiffusion()[i+1][j];
				kill = dp.getKilling()[i+1][j];
				
//				if( ((mu * h) / (2 * sig)) > 1 ) {
//					System.out.println("Unstable: " + ((mu * h) / (2 * sig)));
//				}
//				
//				if( ((sig * h) / (k * k)) > 0.5 ) {
//					System.out.println("Unstable: " + ((sig * h) / (k * k)) );
//				}
				
//				 + "h = ( tmax-tmin ) / m ; \n"
//				 + "k = ( xmax-xmin ) / n ; \n\n"
				
				solution[i+1][j+1]=
						// Term (i-1)
						( (-mu * h) / (2 * k) + ( ((sig * sig)/2) * h ) / (k * k) ) * solution[i][j] 
						
						// Term (i)
						+ (1-(kill * h)+ ( (-2 * ((sig * sig)/2) * h) / (k * k) )) * solution[i+1][j] 
						
						// Term (i+1)
						+ ( (mu * h) / (2 * k) + ( ((sig * sig)/2) * h ) / (k * k) ) * solution[i+2][j];
			}
		}
	}
		
	// Forgot what this does - Round to three digit precision?
	public void setPoints()
	{
		for(int j=0; j<=m; j++)
		{
			for(int i=0; i<=n-1; i++)
			{
				points[i][j] = Math.round(solution[i][j]*1000d)/1000d;
			}
		}
	}
	
	// Getters go here
	// TODO create all getters for the graph class
	
	public double gettMin() {
		return tmin;
	}
	
	public double gettMax() {
		return tmax;
	}
	
	public double getxMin() {
		return xmin;
	}
	
	public double getxMax() {
		return xmax;
	}
	
	public int getN() {
		return n;
	}
	
	public int getM() {
		return m;
	}
	
	public double[][] getSolution() {
		return solution;
	}
	
	public double[] getX() {
		return x;
	}
	
	public double[] getT() {
		return t;
	}
}