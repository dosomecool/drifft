package application.prototypes.survivalpde;

import java.io.IOException;

import application.prototypes.main.CSVable;
import application.prototypes.main.Constants;
import application.prototypes.main.Helpable;
import application.prototypes.main.PNGable;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

public class FTCSGraph extends BorderPane implements PNGable, CSVable, Helpable {
			
	private double graphWidth = 800;
	private double graphHeight = 480;
	
	@FXML
	private BorderPane ItoGraphPane;
	@FXML
	private AnchorPane ItoGraphCenter;
	
	private Pane ItoDiffusionChartPane;
	private FTCSSolver proc;
	
	private ScatterChart<Number, Number> pathChart;

	private static ObservableList<XYChart.Series<Number, Number>> pathSeries = FXCollections.observableArrayList();

	public FTCSGraph(FTCSSolver solver) 
	{
		// CONSTRUCTOR
		try {			
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource(Constants.FXML_RESOURCE_PATH + "ItoGraph.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (solver != null){
			proc = solver;
		}
		
		pathSeries.clear();
		getItoDiffusionChartPane();
		
		ItoGraphPane.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "ItoGraphStyle.css").toExternalForm());
		
		// ItoGraphPane.setId("SURVIVAL_PDE");
	}

	public void getItoDiffusionChartPane()
	{
		ItoDiffusionChartPane = new Pane();
		
		// Define the axes
	    final NumberAxis xAxis = new NumberAxis("Time, t",  proc.gettMin(), proc.gettMax(), 1);
	    final NumberAxis yAxis = new NumberAxis("State, x", proc.getxMin(),  proc.getxMax(), 1);
	    
	    // PATH CHART
	    pathChart = new ScatterChart<Number,Number>(xAxis,yAxis);
	    
	    // PATH CHART PROPERTIES
	    pathChart.setLegendVisible(false);
	    pathChart.setAnimated(false);
	    
	    pathChart.setHorizontalGridLinesVisible(false);
	    pathChart.setHorizontalZeroLineVisible(false);
	    
	    pathChart.setVerticalGridLinesVisible(false);
	    pathChart.setVerticalZeroLineVisible(false);
	    pathChart.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "PdeLineChartStyle.css").toExternalForm());
	    
	    pathChart.setMinSize(graphWidth, graphHeight);
	    
	    // PATH CHART SERIES
	    for (int n=0; n < 11  ; n++)
		{
	    	pathSeries.add(new Series<Number, Number>());
		}
	  
        pathChart.getData().addAll(pathSeries);
        	    
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        
        AnchorPane.setBottomAnchor(ItoDiffusionChartPane, (double) 0);
        AnchorPane.setTopAnchor(ItoDiffusionChartPane, (double) 20);
        AnchorPane.setLeftAnchor(ItoDiffusionChartPane, (double) 0);
        AnchorPane.setRightAnchor(ItoDiffusionChartPane, (double) 0);
        
        // ADD ALL TO PANE - THIS IS DONE FOR ALL CHARTS
        ItoDiffusionChartPane.getChildren().addAll(pathChart);
        ItoGraphCenter.getChildren().setAll(ItoDiffusionChartPane);
	}
	
	// PATH DATA TASK
	Task<Void> addPathDataTask = new Task<Void>() {

		@Override 
		public Void call() {
			
			// PATH SERIES
			for (int n=0; n<= proc.getN(); n++) // proc.getN()+1
			{
				for(int m=0; m<= proc.getM(); m++) // proc.getM()
				{	
					int space = n;
					int time = m;

					Platform.runLater(new Runnable() {
						@Override
						public void run() {

							try {
								double solution = proc.getSolution()[space][time];
								
								if(solution >= 0.9){
									pathSeries.get(0).getData().add(new XYChart.Data<Number, Number>(proc.getT()[time], 
											proc.getX()[space]));	

								} else if(solution >= 0.8 && solution < 0.9){
									pathSeries.get(1).getData().add(new XYChart.Data<Number, Number>(proc.getT()[time], 
											proc.getX()[space]));

								} else if(solution >= 0.7 && solution < 0.8){
									pathSeries.get(2).getData().add(new XYChart.Data<Number, Number>(proc.getT()[time], 
											proc.getX()[space]));

								} else if(solution >= 0.6 && solution < 0.7){
									pathSeries.get(3).getData().add(new XYChart.Data<Number, Number>(proc.getT()[time], 
											proc.getX()[space]));

								} else if(solution >= 0.5 && solution < 0.6){
									pathSeries.get(4).getData().add(new XYChart.Data<Number, Number>(proc.getT()[time], 
											proc.getX()[space]));

								} else if(solution >= 0.4 && solution < 0.5){
									pathSeries.get(5).getData().add(new XYChart.Data<Number, Number>(proc.getT()[time], 
											proc.getX()[space]));
									
								} else if(solution >= 0.3 && solution < 0.4){
									pathSeries.get(6).getData().add(new XYChart.Data<Number, Number>(proc.getT()[time], 
											proc.getX()[space]));
									
								} else if(solution >= 0.2 && solution < 0.3){
									pathSeries.get(7).getData().add(new XYChart.Data<Number, Number>(proc.getT()[time], 
											proc.getX()[space]));
									
								} else if(solution >= 0.1 && solution < 0.2){
									pathSeries.get(8).getData().add(new XYChart.Data<Number, Number>(proc.getT()[time], 
											proc.getX()[space]));
									
								} else if(solution >= 0.0 && solution < 0.1){
									pathSeries.get(9).getData().add(new XYChart.Data<Number, Number>(proc.getT()[time], 
											proc.getX()[space]));
									
								} else {
									pathSeries.get(10).getData().add(new XYChart.Data<Number, Number>(proc.getT()[time], 
											proc.getX()[space]));
								}
								
							}catch (ArrayIndexOutOfBoundsException A){
								// TODO
							}
						}
					});
					
					if (isCancelled()) {
		                break;
		            }
				}
			}
			return null;
		}
		@Override 
		protected void running() {
			super.succeeded();
			updateMessage("RUNNING!");
		}

		@Override 
		protected void succeeded() {
			super.succeeded();
			updateMessage("DONE!");
		}

		@Override 
		protected void cancelled() {
			super.cancelled();
			updateMessage("CANCELLED!");
		}

		@Override 
		protected void failed() {
			super.failed();
			updateMessage("FAILED!");
		}
	};
	
//	public double halina(int i){
//
//		return (Math.sqrt(0.5)/2) * Math.tanh(Math.sqrt(0.5)*proc.getT()[i]) 
//				* Math.pow(Math.cosh(Math.sqrt(0.5)*proc.getT()[i]),-0.5);
//	}
	
	public AnchorPane getItoGraphCenter() {
		return ItoGraphCenter;
	}
	
	public Task<Void> getPathDataTask(){
		return addPathDataTask;
	} 
	
	public ScatterChart<Number, Number> getPathChart() {
		return pathChart;
	}

	public static ObservableList<XYChart.Series<Number, Number>> getPathSeries() {
		return pathSeries;
	}
	
	// TO PNG
	@Override
	public Node getChart() {
		return ItoDiffusionChartPane;
	}

	// TO CSV
	@Override
	public String[][] getData() {
		
		int n = proc.getN();
		int m = proc.getM();
		String[][] sdata = new String[n+1][m+1];
		
		for (int i = 0; i<=n; i++)
		{
			for (int j=0; j<=m; j++)
			{
				sdata[i][j] = Double.toString(proc.getSolution()[i][j]);
			}
		}
		return sdata;
	}

	@Override
	public int getPageNumber() {
		return 1;
	}
}

