package application.prototypes.density;

import java.io.IOException;

import application.prototypes.diffusion.ItoSolver;
import application.prototypes.main.CSVable;
import application.prototypes.main.Constants;
import application.prototypes.main.Helpable;
import application.prototypes.main.PNGable;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class DiffusionDensityGraph extends BorderPane implements PNGable, CSVable, Helpable{
	
	public static final String FXML_RESOURCE_PATH = "/application/prototypes/fxml/";
	
	private double graphWidth = 800;
	private double graphHeight = 480;
	
	private int numPath;
	
	@FXML
	private BorderPane ItoGraphPane;
	@FXML
	private AnchorPane ItoGraphCenter;
	@FXML
	private Button generateButton;
	
	@FXML
	private Label meanStatusLabel;
	@FXML
	private CheckBox meanCheckBox;
	
	private Pane ItoDensityChartPane;
	private ItoSolver proc;
	
	@FXML
	private GridPane legend;
	@FXML
	private Label pathLegendLabel, meanLegendLabel, varianceLegendLabel;
	
	@FXML
	private Separator pathSymbol, meanSymbol, varianceSymbol;
	
	private static ObservableList<XYChart.Series<Number, Number>> meanSeries = FXCollections.observableArrayList();
	
	private AreaChart<Number,Number> pathMeanChart;
	
	public DiffusionDensityGraph(ItoSolver xItoDiffusion) 
	{
		// Constructor body
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource(Constants.FXML_RESOURCE_PATH + "ItoGraph.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (xItoDiffusion != null){
			proc = xItoDiffusion;
		}
		
		meanSeries.clear();
		getItoDiffusionChartPane();
		
		numPath = proc.getNumPath();
		
		ItoGraphPane.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "ItoGraphStyle.css").toExternalForm());
		
		// ItoGraphPane.setId("DENSITY");
	}

	public void getItoDiffusionChartPane()
	{
		ItoDensityChartPane = new Pane();
		
		// AXES
		final NumberAxis xAxis = new NumberAxis("Time, t", proc.gettMin(), proc.gettMax(), 1);
	    final NumberAxis yAxis = new NumberAxis("Density", 0d,  proc.getDensityMax(), 0.1);
	    
	    // PATH CHART
	    pathMeanChart = new AreaChart<Number,Number>(xAxis,yAxis);
	    
	    // PATH CHART PROPERTIES
	    pathMeanChart.setLegendVisible(false);
	    pathMeanChart.setAnimated(false);
	    pathMeanChart.setCreateSymbols(false);
	    
	    pathMeanChart.setHorizontalGridLinesVisible(false);
	    pathMeanChart.setHorizontalZeroLineVisible(false);
	    
	    pathMeanChart.setVerticalGridLinesVisible(false);
	    pathMeanChart.setVerticalZeroLineVisible(false);
	    
	    pathMeanChart.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "MeanPathLineChartStyle.css").toExternalForm());
	    
	    pathMeanChart.setMinSize(graphWidth, graphHeight);
	    pathMeanChart.setAnimated(false);
	    
	    // PATH CHART SERIES
	    meanSeries.add(new Series<Number, Number>());
        meanSeries.get(0).setName("DENSITY");
	    
        pathMeanChart.getData().addAll(meanSeries);
	    
        final DropShadow shadow = new DropShadow();
        shadow.setOffsetX(3);
        shadow.setColor(Color.GREY);
        pathMeanChart.setEffect(shadow);
        
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        
        // VARIANCE AREA CHART
	    final AreaChart<Number,Number> pathVarianceAreaChart = 
	    		new AreaChart<Number,Number>(xAxis,yAxis);
	    
	    // VARIANCE AREA CHART PROPERTIES - USED ONLY AS BACKGROUND
	    pathVarianceAreaChart.setLegendVisible(false);
	    pathVarianceAreaChart.setAnimated(false);
	    pathVarianceAreaChart.setCreateSymbols(false);
	   
	    pathVarianceAreaChart.setHorizontalGridLinesVisible(false);
	    pathVarianceAreaChart.setHorizontalZeroLineVisible(false);
	    
	    pathVarianceAreaChart.setVerticalGridLinesVisible(false);
	    pathVarianceAreaChart.setVerticalZeroLineVisible(false);
	    
	    pathVarianceAreaChart.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "VarianceAreaChartStyle.css").toExternalForm());
	    
	    pathVarianceAreaChart.setMinSize(graphWidth, graphHeight);
     
	    // NO SERIES 
        
        AnchorPane.setBottomAnchor(ItoDensityChartPane, (double) 0);
        AnchorPane.setTopAnchor(ItoDensityChartPane, (double) 20);
        AnchorPane.setLeftAnchor(ItoDensityChartPane, (double) 0);
        AnchorPane.setRightAnchor(ItoDensityChartPane, (double) 0);
        
        // ADD ALL TO PANE - THIS IS DONE FOR ALL CHARTS
        ItoDensityChartPane.getChildren().addAll(pathVarianceAreaChart, pathMeanChart);
        ItoGraphCenter.getChildren().setAll(ItoDensityChartPane);
	}
	
	// MEAN DATA CHART
	Task<Void> addMeanDataTask = new Task<Void>() {

		@Override 
		public Void call() {

			for(int i=0; i<=proc.getM(); i++)
			{
				int dataIndex = i;
				Platform.runLater(new Runnable() {

					@Override
					public void run() {

						// Code here
						meanSeries.get(0).getData().add(new XYChart.Data<Number, Number>(proc.getT()[dataIndex], 
								proc.getDensity()[dataIndex]));
					}
				});
				if (isCancelled()) {
	                updateMessage("Cancelled");
	                break;
	            }
			}
			return null;
		}
		@Override 
		protected void running() {
			super.succeeded();
			updateMessage("RUNNING!");
		}

		@Override 
		protected void succeeded() {
			super.succeeded();
			updateMessage("DONE!");
		}

		@Override 
		protected void cancelled() {
			super.cancelled();
			updateMessage("CANCELLED!");
		}

		@Override 
		protected void failed() {
			super.failed();
			updateMessage("FAILED!");
		}
	};
	
	public AnchorPane getItoGraphCenter() {
		return ItoGraphCenter;
	}

	public Task<Void> getMeanDataTask(){
		return addMeanDataTask;
	}
	
	public AreaChart<Number,Number> getPathMeanChart() {
		return pathMeanChart;
	}
	
	public static ObservableList<XYChart.Series<Number, Number>> getMeanSeries() {
		return meanSeries;
	}
	
	public int getNumPath() {
		return numPath;
	}

	// TO PNG
	@Override
	public Node getChart() {
		return ItoDensityChartPane;
	}
	
	// TO CSV
	@Override
	public String[][] getData() {
		
		int n = proc.getNumPath();
		int m = proc.getM();
		String[][] sdata = new String[1][m+1];
		
		for (int j=0; j<=m; j++)
		{
			sdata[0][j] = Double.toString(proc.getDensity()[j]);
		}
		return sdata;
	}

	@Override
	public int getPageNumber() {
		return 0;
	}
}