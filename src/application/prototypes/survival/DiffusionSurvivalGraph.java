package application.prototypes.survival;

import java.io.IOException;

import application.prototypes.diffusion.ItoSolver;
import application.prototypes.main.CSVable;
import application.prototypes.main.Constants;
import application.prototypes.main.Helpable;
import application.prototypes.main.PNGable;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

public class DiffusionSurvivalGraph extends BorderPane implements PNGable, CSVable, Helpable{

	private double graphWidth = 800;
	private double graphHeight = 480;
	
	private int numPath;
	
	@FXML
	private BorderPane ItoGraphPane;
	@FXML
	private AnchorPane ItoGraphCenter;
	@FXML
	private Button generateButton;
	@FXML
	private Label pathStatusLabel, meanStatusLabel, varianceAreaStatusLabel, 
			varianceLineStatusLabel;
	@FXML
	private CheckBox pathCheckBox, meanCheckBox, varianceLineCheckBox, 
			varianceAreaCheckBox;
	
	private Pane ItoSurvivalChartPane;

	private ItoSolver proc;

	@FXML
	private ProgressBar progressBar;

	private LineChart<Number,Number> pathChart;
	private AreaChart<Number,Number> pathVarianceAreaChart;
	private LineChart<Number,Number> pathVarianceLineChart;
	private LineChart<Number,Number> pathMeanChart;

	private static ObservableList<XYChart.Series<Number, Number>> pathSeries = FXCollections.observableArrayList();
	private static ObservableList<XYChart.Series<Number, Number>> meanSeries = FXCollections.observableArrayList();
	private static ObservableList<XYChart.Series<Number, Number>> varianceLineSeries = FXCollections.observableArrayList();
	private static ObservableList<XYChart.Series<Number, Number>> varianceAreaSeries = FXCollections.observableArrayList();

	public DiffusionSurvivalGraph(ItoSolver xItoDiffusion) 
	{
		// Constructor body
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource(Constants.FXML_RESOURCE_PATH + "ItoGraph.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (xItoDiffusion != null){
			proc = xItoDiffusion;
		}

		pathSeries.clear();
		meanSeries.clear();
		varianceLineSeries.clear();
		varianceAreaSeries.clear();

		getItoSurvivalChartPane();
		
		numPath = proc.getNumPath();
		
		ItoGraphPane.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "ItoGraphStyle.css").toExternalForm());
		
		// ItoGraphPane.setId("SURVIVAL");
	}

	public void getItoSurvivalChartPane()
	{
		ItoSurvivalChartPane = new Pane();

		// AXES	    
		final NumberAxis xAxis = new NumberAxis("Time, t",  proc.gettMin(), proc.gettMax(), 1);
		final NumberAxis yAxis = new NumberAxis("Survival", 0d,  1d, 0.1);

		// PATH CHART
		pathChart = new LineChart<Number,Number>(xAxis,yAxis);

		// PATH CHART PROPERTIES
		pathChart.setLegendVisible(false);
		pathChart.setAnimated(false);
		pathChart.setCreateSymbols(false);

		pathChart.setHorizontalGridLinesVisible(false);
		pathChart.setHorizontalZeroLineVisible(false);

		pathChart.setVerticalGridLinesVisible(false);
		pathChart.setVerticalZeroLineVisible(false);
		pathChart.getStylesheets().addAll(getClass()
				.getResource(Constants.CSS_RESOURCE_PATH + "PathLineChartStyle.css").toExternalForm());

		pathChart.setMinSize(graphWidth, graphHeight);
		pathChart.setAnimated(false);

		// PATH CHART SERIES
		for (int n=0; n<proc.getNumPath(); n++)
		{
			pathSeries.add(new Series<Number, Number>());
		}

		pathChart.getData().addAll(pathSeries);

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		// VARIANCE AREA CHART
		pathVarianceAreaChart = new AreaChart<Number,Number>(xAxis,yAxis);

		// VARIANCE AREA CHART PROPERTIES
		pathVarianceAreaChart.setLegendVisible(false);
		pathVarianceAreaChart.setAnimated(false);
		pathVarianceAreaChart.setCreateSymbols(false);

		pathVarianceAreaChart.setHorizontalGridLinesVisible(false);
		pathVarianceAreaChart.setHorizontalZeroLineVisible(false);

		pathVarianceAreaChart.setVerticalGridLinesVisible(false);
		pathVarianceAreaChart.setVerticalZeroLineVisible(false);

		pathVarianceAreaChart.getStylesheets().addAll(getClass()
				.getResource(Constants.CSS_RESOURCE_PATH + "VarianceAreaChartStyle.css").toExternalForm());

		pathVarianceAreaChart.setMinSize(graphWidth, graphHeight);
		pathVarianceAreaChart.setAnimated(false);

		// VARIANCE AREA CHART SERIES - TWO SERIES TOTAL #0 and #1
		varianceAreaSeries.add(new Series<Number, Number>());
		varianceAreaSeries.add(new Series<Number, Number>());
		pathVarianceAreaChart.getData().addAll(varianceAreaSeries);

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		// VARIANCE LINE CHART
		pathVarianceLineChart = new LineChart<Number,Number>(xAxis,yAxis);

		// VARIANCE LINE CHART PROPERTIES
		pathVarianceLineChart.setLegendVisible(false);
		pathVarianceLineChart.setAnimated(false);
		pathVarianceLineChart.setCreateSymbols(false);

		pathVarianceLineChart.setHorizontalGridLinesVisible(false);
		pathVarianceLineChart.setHorizontalZeroLineVisible(false);

		pathVarianceLineChart.setVerticalGridLinesVisible(false);
		pathVarianceLineChart.setVerticalZeroLineVisible(false);

		pathVarianceLineChart.getStylesheets().addAll(getClass()
				.getResource(Constants.CSS_RESOURCE_PATH + "VarianceLineChartStyle.css").toExternalForm());

		pathVarianceLineChart.setMinSize(graphWidth, graphHeight);
		pathVarianceLineChart.setAnimated(false);

		// VARIANCE LINE CHART SERIES - TWO SERIES TOTAL #0 and #1
		varianceLineSeries.add(new Series<Number, Number>());
		varianceLineSeries.add(new Series<Number, Number>());
		pathVarianceLineChart.getData().addAll(varianceLineSeries);

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		// MEAN CHART
		pathMeanChart = new LineChart<Number,Number>(xAxis,yAxis);

		// MEAN CHART POPERTIES
		pathMeanChart.setLegendVisible(false);
		pathMeanChart.setAnimated(false);
		pathMeanChart.setCreateSymbols(false);

		pathMeanChart.setHorizontalGridLinesVisible(false);
		pathMeanChart.setHorizontalZeroLineVisible(false);

		pathMeanChart.setVerticalGridLinesVisible(false);
		pathMeanChart.setVerticalZeroLineVisible(false);

		pathMeanChart.getStylesheets().addAll(getClass()
				.getResource(Constants.CSS_RESOURCE_PATH + "MeanPathLineChartStyle.css").toExternalForm());

		pathMeanChart.setMinSize(graphWidth, graphHeight);
		pathMeanChart.setAnimated(false);

		// MEAN CHART SERIES
		meanSeries.add(new Series<Number, Number>());
		pathMeanChart.getData().addAll(meanSeries);
		
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		AnchorPane.setBottomAnchor(ItoSurvivalChartPane, (double) 0);
		AnchorPane.setTopAnchor(ItoSurvivalChartPane, (double) 20);
		AnchorPane.setLeftAnchor(ItoSurvivalChartPane, (double) 0);
		AnchorPane.setRightAnchor(ItoSurvivalChartPane, (double) 0);

		// ADD ALL TO PANE - THIS IS DONE FOR ALL CHARTS
		ItoSurvivalChartPane.getChildren().addAll(pathVarianceAreaChart, 
        		pathChart, pathVarianceLineChart, pathMeanChart);
		ItoGraphCenter.getChildren().setAll(ItoSurvivalChartPane);
	}

	// PATH DATA TASK
	Task<Void> addPathDataTask = new Task<Void>() {

		@Override 
		public Void call() {

			double numPath;
			
			if (proc.getNumPath() > 100) {
				numPath = 100;
			} else {
				numPath = proc.getNumPath();
			}
			
			// PATH SERIES
			for (int n=0; n<numPath; n++)
			{
				for(int i=0; i<=proc.getM(); i++)
				{	
					int pathNumber = n;
					int dataIndex = i;

					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							pathSeries.get(pathNumber).getData().add(new XYChart.Data<Number, Number>(proc.getT()[dataIndex], 
									proc.getSurvival()[pathNumber][dataIndex]));
						}
					});
					if (isCancelled()) {
						updateMessage("Cancelled");
						break;
					}
				}
			}
			return null;
		}
		@Override 
		protected void running() {
			super.succeeded();
			updateMessage("RUNNING!");
		}

		@Override 
		protected void succeeded() {
			super.succeeded();
			updateMessage("DONE!");
		}

		@Override 
		protected void cancelled() {
			super.cancelled();
			updateMessage("CANCELLED!");
		}

		@Override 
		protected void failed() {
			super.failed();
			updateMessage("FAILED!");
		}
	};

	// VARIANCE AREA DATA TASK
	Task<Void> addVarianceAreaDataTask = new Task<Void>() {

		@Override 
		public Void call() {

			for(int i=0; i<proc.getM(); i++)
			{
				int dataIndex = i;
				Platform.runLater(new Runnable() {

					@Override
					public void run() {

						varianceAreaSeries.get(0).getData().add(new XYChart.Data<Number, Number>(proc.getT()[dataIndex ], 
								proc.getMeanSurvival()[dataIndex ] + proc.getVarianceSurvival()[dataIndex]));

						varianceAreaSeries.get(1).getData().add(new XYChart.Data<Number, Number>(proc.getT()[dataIndex ], 
								proc.getMeanSurvival()[dataIndex ] - proc.getVarianceSurvival()[dataIndex]));
					}
				});
				if (isCancelled()) {
					updateMessage("Cancelled");
					break;
				}
			}
			return null;
		}
		@Override 
		protected void running() {
			super.succeeded();
			updateMessage("RUNNING!");
		}

		@Override 
		protected void succeeded() {
			super.succeeded();
			updateMessage("DONE!");
		}

		@Override 
		protected void cancelled() {
			super.cancelled();
			updateMessage("CANCELLED!");
		}

		@Override 
		protected void failed() {
			super.failed();
			updateMessage("FAILED!");
		}
	};

	// VARIANCE LINE DATA TASK
	Task<Void> addVarianceLineDataTask = new Task<Void>() {

		@Override 
		public Void call() {

			for(int i=0; i<proc.getM(); i++)
			{
				int dataIndex = i;
				Platform.runLater(new Runnable() {

					@Override
					public void run() {

						varianceLineSeries.get(0).getData().add(new XYChart.Data<Number, Number>(proc.getT()[dataIndex ], 
								proc.getMeanSurvival()[dataIndex ] + proc.getVarianceSurvival()[dataIndex]));

						varianceLineSeries.get(1).getData().add(new XYChart.Data<Number, Number>(proc.getT()[dataIndex ], 
								proc.getMeanSurvival()[dataIndex ] - proc.getVarianceSurvival()[dataIndex]));
					}
				});
				if (isCancelled()) {
					updateMessage("Cancelled");
					break;
				}
			}
			return null;
		}
		@Override 
		protected void running() {
			super.succeeded();
			updateMessage("RUNNING!");
		}

		@Override 
		protected void succeeded() {
			super.succeeded();
			updateMessage("DONE!");
		}

		@Override 
		protected void cancelled() {
			super.cancelled();
			updateMessage("CANCELLED!");
		}

		@Override 
		protected void failed() {
			super.failed();
			updateMessage("FAILED!");
		}
	};

	// MEAN DATA CHART
	Task<Void> addMeanDataTask = new Task<Void>() {

		@Override 
		public Void call() {

			for(int i=0; i<proc.getM(); i++)
			{
				int dataIndex = i;
				Platform.runLater(new Runnable() {

					@Override
					public void run() {

						// Code here
						meanSeries.get(0).getData().add(new XYChart.Data<Number, Number>(proc.getT()[dataIndex], 
								proc.getMeanSurvival()[dataIndex]));
					}
				});
				if (isCancelled()) {
					updateMessage("Cancelled");
					break;
				}
			}
			return null;
		}
		@Override 
		protected void running() {
			super.succeeded();
			updateMessage("RUNNING!");
		}

		@Override 
		protected void succeeded() {
			super.succeeded();
			updateMessage("DONE!");
		}

		@Override 
		protected void cancelled() {
			super.cancelled();
			updateMessage("CANCELLED!");
		}

		@Override 
		protected void failed() {
			super.failed();
			updateMessage("FAILED!");
		}
	};
	
	public AnchorPane getItoGraphCenter() {
		return ItoGraphCenter;
	}
	
	public Task<Void> getPathDataTask(){
		return addPathDataTask;
	} 
	
	public Task<Void> getVarianceAreaDataTask(){
		return addVarianceAreaDataTask;
	} 
	
	public Task<Void> getVarianceLineDataTask (){
		return addVarianceLineDataTask;
	}
	
	public Task<Void> getMeanDataTask(){
		return addMeanDataTask;
	}
	
	public LineChart<Number, Number> getPathChart() {
		return pathChart;
	}
	
	public LineChart<Number, Number> getPathVarianceLineChart() {
		return pathVarianceLineChart;
	}
	
	public AreaChart<Number, Number> getPathVarianceAreaChart() {
		return pathVarianceAreaChart;
	}
	
	public LineChart<Number, Number> getPathMeanChart() {
		return pathMeanChart;
	}
	
	public static ObservableList<XYChart.Series<Number, Number>> getPathSeries() {
		return pathSeries;
	}
	
	public static ObservableList<XYChart.Series<Number, Number>> getVarianceAreaSeries() {
		return varianceAreaSeries;
	}
	
	public static ObservableList<XYChart.Series<Number, Number>> getVarianceLineSeries() {
		return varianceLineSeries;
	}
	
	public static ObservableList<XYChart.Series<Number, Number>> getMeanSeries() {
		return meanSeries;
	}
	
	public int getNumPath() {
		return numPath;
	}

	// TO PNG
	@Override
	public Node getChart() {
		return ItoSurvivalChartPane;
	}
	
	// TO CSV
	@Override
	public String[][] getData() {
		
		int n = proc.getNumPath();
		int m = proc.getM();
		String[][] sdata = new String[n][m+1];
		
		for (int index = 0; index < n; index++)
		{
			for (int j=0; j<=m; j++)
			{
				sdata[index][j] = Double.toString(proc.getSurvival()[index][j]);
			}
		}
		return sdata;
	}

	// TO HELP PAGE
	@Override
	public int getPageNumber() {
		return 0;
	}
}
