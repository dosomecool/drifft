package application.prototypes;

public class ItoCalculus {

	private Diffusion dynamicDiffusion;
	
	// numPath, h, m, time, xState, drift, diffusion, kill

	// * NUMBER OF PATHS
	private int numPath;
	
	// * DELTA T
	private double h;
	
	// * TEMPORAL INTERVALS
	private int m;
	
	// * TIME INDEX
	private double[] t;
	
	// * DRIFT FUNCTION ARRAY
	private double[][] drift;
	
	// * DRIFT FUNCTION ARRAY
	private double[][] diffusion;
	
	// * KILLING FUNCTION ARRAY
	private double[][] kill;
	
	// * X 
	private double[][] x;
	private double[] meanX;
	private double[] varianceX;
	
	// X-STAR
	private double[][] xStar;
	private double[] meanxStar;
	private double[] variancexStar;

	// SURVIVAL
	private double[][] survival;
	private double[] meanSurvival;
	private double[] varianceSurvival;
	
	// DENSITY
	private double[] density;
	
	ItoCalculus() 
	{
		dynamicDiffusion = new Diffusion();
		
		// * NUMBER OF PATHS
		numPath = dynamicDiffusion.getNumPaths();
		
		// * DELTA T
		h = dynamicDiffusion.getH();
		
		// * TEMPORAL INTERVALS
		m = dynamicDiffusion.getM();
		
		// * TIME INDEX
		t = dynamicDiffusion.getTime();
		
		// * DRIFT FUNCTION ARRAY
		drift = dynamicDiffusion.getDrift();
		
		// * DRIFT FUNCTION ARRAY
		diffusion = dynamicDiffusion.getDiffusion();
		
		// * KILLING FUNCTION ARRAY
		kill = dynamicDiffusion.getKill();
		
		// X STATE
		x = dynamicDiffusion.getxState();
		meanX = new double[m+1];
		varianceX = new double[m+1];
		
		// X-STAR STATE
		xStar = dynamicDiffusion.getxStar();
		meanxStar = new double[m+1];
		variancexStar = new double[m+1];
		
		// SURVIVAL STATE
		survival = new double[numPath][m+1];
		meanSurvival = new double[m+1];
		varianceSurvival = new double[m+1];
		
		// DENSITY STATE
		density = new double[m+1];
		
		// TODO: DO WE NEED TO RUN EVERYTHING?
		// RUNS EVERYTHING
		
        meanX();
        varianceX();

        survival();
        meanSurvival();
        varianceSurvival();
        
        itoDensity();
        
        meanxStar();
        variancexStar();

        getLimits();
        
//        itoCumulativeDistribution();
	}
	
	// ******************** X STATE METHODS ********************
	
	public void meanX()
	{	
		for(int j=0; j<=m; j++)
		{
			double meanSum = 0;
			
			for(int index = 0; index < numPath; index++ )
			{
				meanSum = meanSum + x[index][j];
			}
			meanX[j] = meanSum / numPath;
		}
	}
	
	public void varianceX()
	{	
		for(int j=0; j<=m; j++)
		{
			double varianceSum = 0;
			
			for(int index = 0; index < numPath; index++ )
			{
				varianceSum = varianceSum + 
						(( x[index][j] - meanX[j] ) * ( x[index][j] - meanX[j] ));
			}
			
			varianceX[j] = Math.sqrt(varianceSum / numPath);
		}
	}
	
	// ******************** X-STAR STATE METHODS ********************
	
	// X STAR

	public void meanxStar() {	
		
		for (int j=0; j<=m; j++)
		{
			double meanSum = 0;

			for (int index = 0; index < numPath; index++ )
			{
				meanSum = meanSum + xStar[index][j];
			}
			meanxStar[j] = meanSum / numPath;
		}
	}
	
	public void variancexStar()
	{	
		for (int j=0; j<=m; j++)
		{
			double varianceSum = 0;
			
			for (int index = 0; index < numPath; index++ )
			{
				varianceSum = varianceSum + 
						(( xStar[index][j] - meanxStar[j] ) * ( xStar[index][j] - meanxStar[j] ));
			}
			
			variancexStar[j] = Math.sqrt(varianceSum / numPath);
		}
	}
	
	// ******************** SURVIVAL STATE METHODS ********************
	

	public void survival() 
	{
		for (int index = 0; index < numPath; index++ )
		{
			survival[index][0]=1;
					
			for (int j=0; j<=m-1; j++)
			{
				survival[index][j+1] = (1 - kill[index][j] * h) * survival[index][j];
			}
		}
	}
	
	public void meanSurvival()
	{	
		for (int j=0; j<=m; j++)
		{
			double meanSum = 0;
		
			for (int index = 0; index < numPath; index++)
			{
				meanSum = meanSum + survival[index][j];
			}
			meanSurvival[j] = meanSum / numPath;
		}
	}
	
	public void varianceSurvival()
	{
		for(int j=0; j<=m; j++)
		{
			double varianceSum = 0;
			
			for(int index = 0; index < numPath; index++ )
			{
				varianceSum = varianceSum + 
						(( survival[index][j] - meanSurvival[j] ) * 
								( survival[index][j] - meanSurvival[j] ));
			}
			varianceSurvival[j] = Math.sqrt(varianceSum / numPath);
		}
	}
	
	// ******************** DENSITY STATE METHODS ********************
	
	private double densityMax = 0;
	
	public void itoDensity() 
	{
		density[0] = 0;
		
		for(int j=1; j<=m-1; j++)
		{
			density[j+1] = ( meanSurvival[j-1] - meanSurvival[j+1]) / (2*h);
			
			// Stores the highest x value
			if (density[j+1] > densityMax){
				densityMax = density[j+1];
			}
		}
	}
	
	
	// TODO: IS THIS THE BEST WAY TO DO THIS ?
	private double tMin;
	private double tMax;
	private double xMin = 0;
	private double xMax = 0;
	private double xStarMin = 0;
	private double xStarMax = 0;

	public void getLimits(){
		
		tMin = t[0];
		tMax = t[t.length-1];
		
		for(int index = 0; index < numPath; index++ )
		{			
			for(int j=0; j<=m; j++)
			{
				// Stores the lowest x value
				if (x[index][j] < xMin){
					xMin = x[index][j];
				}
				// Stores the highest x value
				if (x[index][j] > xMax){
					xMax = x[index][j];
				}
				
//				// Stores the lowest x value
				if (xStar[index][j] < xStarMin){
					xStarMin = xStar[index][j];
				}
//				// Stores the highest x value
				if (xStar[index][j] > xStarMax){
					xStarMax = xStar[index][j];
				}
			}
		}
	}

	double[] itoCumulativeDist;
	public void itoCumulativeDistribution() 
	{
		itoCumulativeDist = new double[m+1];
		itoCumulativeDist[0] = 0;

		for(int j=0; j<=m-1; j++)
		{
			itoCumulativeDist[j+1] = itoCumulativeDist[j] + density[j+1] * h;
		}
	}
	
	/**
	 * Run all methods within this class in proper order.
	 */
	// Consider moving this to the constructor 
	public void runSimulation()
	{
		//TODO
	}
	
	// TEMPORAL INTERVALS
	public int getM() {
		return m;
	}
	
	public int getNumPath() {
		return numPath;
	}
	
	public double getxMin() {
		return xMin;
	}
	
	public double getxMax() {
		return xMax;
	}
	
	public double getxStarMin() {
		return xStarMin;
	}
	
	public double getxStarMax() {
		return xStarMax;
	}
	
	public double gettMin() {
		return tMin;
	}
	
	public double gettMax() {
		return tMax;
	}
	
	public double[] getT(){
		return t;
	}
	
	// X
	public double[][] getX() {
		return x;
	}
	
	public double[] getMeanX() {
		return meanX;
	}
	
	public double[] getVarianceX() {
		return varianceX;
	}
	
	// X STAR
	public double[][] getxStar() {
		return xStar;
	}
	
	public double[] getMeanxStar() {
		return meanxStar;
	}
	
	public double[] getVariancexStar() {
		return variancexStar;
	}
	
	// SURVIVAL
	public double[][] getSurvival() {
		return survival;
	}
	
	public double[] getMeanSurvival() {
		return meanSurvival;
	}
		
	public double[] getVarianceSurvival() {
		return varianceSurvival;
	}

	// DENSITY
	public double[] getDensity() {
		return density;
	}
	
	public double getDensityMax() {
		return densityMax;
	}
	
	public double[] getItoCumulativeDist() {
		return itoCumulativeDist;
	}
	
	public double getItoLogCumulativeDist(int jIindex) {
		return Math.log(itoCumulativeDist[jIindex]);
	}
}
