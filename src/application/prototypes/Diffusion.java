package application.prototypes;

import java.util.concurrent.Callable;

import application.prototypes.main.Configuration;
import net.openhft.compiler.CompilerUtils;

public class Diffusion {

	// + "survival = Math.round( ((1 - kill * h) * survival) * 10000000000d ) / 10000000000d; \n\n" 
	
	private Configuration config;
	
	// x0, numPath, tmin, tmax, h, drift, diffusion, kill
	
	private String x_0;			         // Initial state
	private String num_paths;            // Number of Paths
	private String t_min;                // Min time
	private String t_max;                // Max tima
	private String _h;                   // dt
	
	private String _mu;                  // Drift form
	private String _sigma;               // Diffusion form
	private String _kill;                // Killing form

	private Object[] diffusionArray;     // Object from Dynamic Class
	
	private String diffusionJavaCode;    // String contains code for Dynamic Class
	
	// DEFAULT CONSTRUCTOR
	public Diffusion(){
		
		config = Configuration.getInstance();
		
		getValues();                                   // Get values from properties file
		setDiffusionJavaCode();
		loadDynamicDiffusionClass();
		getDynamicDiffusionArrays();
	}
	
	public void getValues(){
		
		x_0       = config.getConfig("x0",           "value");
		num_paths = config.getConfig("number_paths", "value");
		t_min     = config.getConfig("limits",       "tmin");
		t_max     = config.getConfig("limits",       "tmax");
		_h        = config.getConfig("delta_time",   "value");
		_mu       = config.getConfig("drift",        "mu");
		System.out.println("_mu: " + _mu);
		_sigma    = config.getConfig("diffusion",    "sigma");
		_kill     = config.getConfig("kill",         "function");
	}
	
	// ******************** DYNAMIC DIFFUSION CLASS ********************
	
	// DIFFUSION STRING CLASS BEGINS 
	String dynamicDiffusionClass = "application.prototypes.DynamicDiffusionClass";
	
	public void setDiffusionJavaCode(){
		
		diffusionJavaCode =  new String( "package application.prototypes; \n\n"

				 		  + "import java.util.concurrent.Callable; \n"
				 		  + "import java.util.Random; \n\n"

		    	 		  + "public final class DynamicDiffusionClass implements Callable<Object[]> { \n\n"

		    	 		  // x0
		    	 		  + "private double x0; \n"

		    	 		  // [0] - numPath
		    	 		  + "private int numPaths; \n\n"

		    	 		  // t
		    	 		  + "private double t; \n"  

		    	 		  // tmin
		    	 		  + "private double tmin; \n"
		    	 		  // tmax
		    	 		  + "private double tmax; \n"

		    	 		  // [1] - h
		    	 		  + "private double h; \n"                   // dt

		    	 		  // [2] - m
		    	 		  + "private int m; \n"                      // temporal interval

		    	 		  // [3] - time
		    	 		  + "private double[] time; \n\n"            // time for each (t)

		    	 		  // x
		    	 		  + "private double x; \n" 

		    	 		  // [4] - xState
		    	 		  + "private double[][] xState; \n\n" 

		    	 		  // [5] - drift
		    	 		  + "private double[][] drift; \n"

		    	 		  // [6] - diffusion
		    	 		  + "private double[][] diffusion; \n"

		    	 		  // [7] - kill
		    	 		  + "private double[][] kill; \n\n"
		    	 		  
		    	 		  // [8] - xStar
		    	 		  + "private double[][] xStar; \n\n"

		    	 		  // TODO: SEED

		    	 		  // x0, numPath, tmin, tmax, h, m, time, xState, drift, diffusion, kill

		    	 		  // BEGIN CONSTRUCTOR
		    	 		  + "public DynamicDiffusionClass() \n"
		    	 		  + "{ \n"

				 	  	  // INSERTED VALUE - *** x0 ***
				 	  	  + "x0 = " + x_0 + "; \n"

				 	  	  // INSERTED VALUE - *** num_paths ***
				 	  	  + "numPaths = " + num_paths + "; \n\n"

				 	  	  // INSERTED VALUE - *** t_min, t_max ***
				 	  	  + "tmin = " + t_min + "; \n"
				 	  	  + "tmax = " + t_max + "; \n"

				 	  	  // INSERTED VALUE - *** h ***
				 	  	  + "h = " + _h + "; \n\n"

				 	  	  // CALCULATES *** m *** BY USING *** tempM *** INTEGER VALUE
				 	  	  + "Double tempM = new Double( ( (tmax-tmin) / h) ); \n"
				 	  	  + "m = tempM.intValue(); \n\n"

				 		  // TIME
				 		  + "time = new double[m+1]; \n\n"

				 		  // STATE X
				 		  + "xState = new double[numPaths][m+1]; \n\n"

				 		  // DRIFT
				 		  + "drift = new double[numPaths][m+1]; \n"

				 		  // DIFFUSION
				 		  + "diffusion = new double[numPaths][m+1]; \n\n"

				 		  // KILL
				 		  + "kill = new double[numPaths][m+1]; \n\n"
				 		  
				 		  // KILL
				 		  + "xStar = new double[numPaths][m+1]; \n\n"

				 		  // CALL METHODS HERE
				 		  + "setTime(); \n"
				 		  + "genPaths(); \n"
				 		  + "setDrift(); \n"
				 		  + "setDiffusion(); \n"
				 		  + "setKilling(); \n"
				 		  + "genxStar(); \n\n"

				 		  + "} \n\n"
				 		  // END CONSTRUCTOR

			     		  + "Random dw = new Random(); \n\n"

			     		  // setTime() - TIME INDEX
			     		  + "public void setTime() \n"
			     		  + "{ \n"

				 	      + "for(int j=0; j<= m; j++) \n"
				 	      + "{ \n"
				 	      + "time[j] = tmin + j * h; \n"

				 		  + "} \n"
				 		  + "} \n\n"

				 		  // genPaths() - REGULAR DIFFUSION DEFINES X STATES
				 		  + "public void genPaths() \n"
				 		  + "{ \n"
				 		  
				 		  + "for(int index = 0; index < numPaths; index++) \n"
				 		  + "{ \n"
				 		  
				 		  // INITIAL STATE
				 		  + "xState[index][0] = x0; \n"

						  + "for(int j=0; j<= m-1 ; j++) \n"
				 		  + "{ \n"
				 		  
				 		  + "t = time[j]; \n"
				 		  + "double x = xState[index][j]; \n"
				 		  
				 		  // INSERTED VALUE - *** mu, sigma ***
				 		  + "xState[index][j+1] = x + " + _mu + " * h + " + _sigma + " * Math.sqrt(h) * dw.nextGaussian(); \n"

				 		  + "} \n"
				 		  + "} \n"
				 		  + "} \n\n"

				 		  
				 		  // genxStar() - GENERATE X STAR METHOD
				 		  + "public void genxStar(){ \n"
				 			  
						  + "for(int index = 0; index < numPaths; index++) \n"
						  + "{ \n"
						 
						  // INITIAL STATE
						  + "xStar[index][0] = x0; \n"
						
						  + "for(int j=0; j<= m-1 ; j++) \n"
						  + "{ \n"
						 
						  + "t = time[j]; \n"
						  + "double x = xStar[index][j]; \n"
						  
						  + "double logdx = getlogdx(x, j); \n"
						  
                          + "// System.out.println(\"logdx: \" + logdx); \n\n"
						  
						  // INSERTED VALUE - *** mu, sigma ***
						  + "xStar[index][j+1] = x + ( " + _mu + " + " + _sigma + " * logdx ) * h + " + _sigma + " * Math.sqrt(h) * dw.nextGaussian(); \n"
						  
						  + "// System.out.println(\"xStar: \" + xStar[index][j+1]); \n\n"
						
						  + "} \n"
						  + "} \n"
				 		  
				 		  + "} \n"  // End of genxStar();
                 		  
				 		  + "private Random dz = new Random(123); \n"
						  
				 		  // begin
						  + "public double getlogdx(double xValue, int tIndex){ \n"
				 	      
						  + "// System.out.println(\"xValue: \" + xValue); \n\n"
						  + "// System.out.println(\"tIndex: \" + tIndex); \n\n"
						  
						  + "double _x1; \n"
						  + "double _x2; \n\n"
						  
						  + "double _kill1; \n"
						  + "double _kill2; \n\n"
						  
						  + "double _survSum1 = 0; \n"
						  + "double _survSum2 = 0; \n\n"
						  
						  + "double _survival1; \n"
						  + "double _survival2; \n\n"
					      
					      + "double _survMean1 = 0; \n"
					      + "double _survMean2 = 0; \n\n"
					      
					      + "double _dxLog = 0; \n\n" 
				 		  
				 		  + "for(int index = 0; index < numPaths; index++) \n"
						  + "{ \n"
				 		  
						  + "_x1 = xValue + 0.001; \n"
						  + "_x2 = xValue - 0.001; \n\n"
						  
						  + "_kill1 = 0; \n"
						  + "_kill2 = 0; \n\n"
						  
						  + "_survival1 = 1; \n"
						  + "_survival2 = 1; \n\n"
						  
						  + "for(int j=0; j<= tIndex; j++) \n"
						  + "{ \n"
						  
						  + "double db = dz.nextGaussian(); \n" 
						  + "// System.out.println(\"db: \" + db); \n\n"
                          + "// System.out.println(\"1 _x1: \" + _x1); \n\n"
                          + "// System.out.println(\"1 _x2: \" + _x2); \n\n"
                          
						  + "_x1 = _x1 + " + _mu.replace("x","_x1") + " * h + " + _sigma.replace("x","_x1") + " * Math.sqrt(h) * db; \n"  
						  + "_x2 = _x2 + " + _mu.replace("x","_x2") + " * h + " + _sigma.replace("x","_x2") + " * Math.sqrt(h) * db; \n\n"
						  
						  + "_kill1 = " + _kill.replace("x","_x1") + "; \n"
						  + "_kill2 = " + _kill.replace("x","_x2") + "; \n\n"
						  
						  + "// System.out.println(\"_kill1: \" + _kill1); \n\n"
						  + "// System.out.println(\"_kill2: \" + _kill2); \n\n"
						  
						  + "_survival1 = (1 - _kill1 * h) * _survival1; \n"
						  + "_survival2 = (1 - _kill2 * h) * _survival2; \n\n"
						  
					      + "// System.out.println(\"_survival1: \" + _survival1); \n\n"
					      + "// System.out.println(\"_survival2: \" + _survival2); \n\n"
						  
						  + "// System.out.println(\"end loop xValue: \" + xValue); \n\n"
						  + "// System.out.println(\"end loop tIndex: \" + tIndex); \n\n"
						  
						  + "} \n" // End of First Loop
						  
						  + "_survSum1 = _survSum1 + _survival1; \n"
						  + "_survSum2 = _survSum2 + _survival2; \n\n"
						  
						  + "} \n" // End of Second Loop
						  
						  + "_survMean1 = _survSum1 / numPaths; \n"
						  + "_survMean2 = _survSum2 / numPaths; \n\n"
				 		  
						  + "_dxLog = ( ( _survMean1 - _survMean2 ) / 0.002 ) / ( ( _survMean1 + _survMean2 ) / 2 ); \n"
				 		  
						  + "return _dxLog; \n"
				 		  + "} \n\n"
                 		  
                 		  
                 		  // setDrift() - DRIFT INDEX
                 		  + "public void setDrift() \n"
                 		  + "{ \n"

				 		  + "for(int index = 0; index < numPaths; index++) \n"
				 		  + "{ \n"
				 		  + "for(int j=0; j<=m; j++) \n"
				 		  + "{ \n"

                 		  + "t = time[j]; "
                 		  + "x = xState[index][j]; \n" 

                 		  // INSERTED VALUE - *** mu ***
                 		  + "drift[index][j] = " + _mu + "; \n"

                 		  + "} \n"
                 		  + "} \n"
                 		  + "} \n\n"

		                  // setDiffusion() - DIFFUSION INDEX
		                  + "public void setDiffusion() \n"
		                  + "{ \n"
		
						  + "for(int index = 0; index < numPaths; index++) \n"
						  + "{ \n"
						  + "for(int j=0; j<= m; j++) \n"
						  + "{ \n"
		
		                  + "t = time[j]; "
		                  + "x = xState[index][j]; \n" 
		
		                  // INSERTED VALUE - *** sigma ***
		                  + "diffusion[index][j] = " + _sigma + "; \n"

                 		  + "} \n"
                          + "} \n"
                          + "} \n\n"

						  // setKilling() - KILLING INDEX
						  + "public void setKilling() \n"
						  + "{ \n"
						  + "for(int index = 0; index < numPaths; index++) \n"
						  + "{ \n"
						  + "for(int j=0; j<=m; j++) \n"
						  + "{ \n"
		
						  + "t = time[j]; \n"
						  + "double x = xState[index][j]; \n"
						  // INSERTED VALUE - *** kill ***
						  + "kill[index][j] = " + _kill + "; \n"
		
						  + "} \n" 		
						  + "} \n"
						  + "} \n\n"
		
						  + "@Override \n"
						  + "public Object[] call() throws Exception { \n"
		
						  // numPath, h, m, time, xState, drift, diffusion, kill
						  + "return new Object[]{numPaths, h, m, time, xState, drift, diffusion, kill, xStar}; \n"
						  + "} \n"
		
						  + "} \n\n" );
	// System.out.print(diffusionJavaCode);
		
	} // STRING CLASS ENDS
		
	@SuppressWarnings("rawtypes")
	private Class diffusionClass;
	private Callable<Object[]> callDiffusion;
	private ClassLoader diffusionClassloader;
	
	@SuppressWarnings("unchecked")
	public void loadDynamicDiffusionClass(){
		
		diffusionClassloader = new ClassLoader() {
		};
		
		try {
			diffusionClass = CompilerUtils.CACHED_COMPILER.loadFromJava(diffusionClassloader, dynamicDiffusionClass, diffusionJavaCode);
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			callDiffusion = (Callable<Object[]>) diffusionClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    public void getDynamicDiffusionArrays(){
    	try {
//    		obj = (Object[]) runner.getClass().newInstance().call();
    		diffusionArray = (Object[]) callDiffusion.call();
    		    		
    	// http://stackoverflow.com/questions/6337075/return-two-arrays-in-a-method-in-java
    		
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		};
    }
    
    // DYNAMIC DIFFUSION GETTERS // numPath, h, m, time, xState, drift, diffusion, kill
    
    public int getNumPaths(){
		return (int) diffusionArray[0];
    }
    
    public double getH(){
		return (double) diffusionArray[1];
    }
    
    public int getM(){
		return (int) diffusionArray[2];
    }
    
    public double[] getTime(){
 		return (double[]) diffusionArray[3];
    }
    
    public double[][] getxState(){
		return (double[][]) diffusionArray[4];
    }
    
    public double[][] getDrift(){
		return (double[][]) diffusionArray[5];
    }
    
    public double[][] getDiffusion(){
		return (double[][]) diffusionArray[6];
    }
    
    public double[][] getKill(){
		return (double[][]) diffusionArray[7];
    }
        
    public double[][] getxStar(){
    	return (double[][]) diffusionArray[8]; 
    }
}
