package application.prototypes.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;

import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import com.sun.tools.javac.code.Attribute.Array;

import application.preloader.hazard.DiffusionHazardGraph;
import application.preloader.hazard.DiffusionHazardGraphPanel;
import application.prototypes.conditioneddiffusion.ConditionedDiffusion;
import application.prototypes.conditioneddiffusion.ConditionedDiffusionGraph;
import application.prototypes.conditioneddiffusion.ConditionedDiffusionGraphPanel;
import application.prototypes.custom.ProcessingPanel;
import application.prototypes.custom.help.Help;
import application.prototypes.density.DiffusionDensityGraph;
import application.prototypes.density.DiffusionDensityGraphPanel;
import application.prototypes.diffusion.Diffusion;
import application.prototypes.diffusion.DiffusionGraph;
import application.prototypes.diffusion.DiffusionGraphPanel;
import application.prototypes.diffusion.ItoSolver;
import application.prototypes.killing.DiffusionKillingGraph;
import application.prototypes.killing.DiffusionKillingGraphPanel;
import application.prototypes.survival.DiffusionSurvivalGraph;
import application.prototypes.survival.DiffusionSurvivalGraphPanel;
import application.prototypes.survivalpde.FTCSGraph;
import application.prototypes.survivalpde.FTCSGraphPanel;
import application.prototypes.survivalpde.FTCSSolver;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Window;

public class MainController extends BorderPane{
	
	private Window owner;
	private ProcessingPanel inProcess = new ProcessingPanel();
	private Help help;
	
	private Timer timer = new Timer(true);
	
	private StringProperty pressedButton = new SimpleStringProperty("NA");
	
	// ******************** FXML COMPONENTS ********************
	
	@FXML
	private MenuBar mainMenuBar;
	@FXML
	private BorderPane mainPane;
	@FXML
	private AnchorPane topMainPane;
	@FXML
	private AnchorPane centerMainPane;
	@FXML
	private AnchorPane leftMainPane;
	@FXML
	private AnchorPane rightMainPane;
	@FXML
	private AnchorPane bottomMainPane;
	@FXML
	private Button diffusionButton, 
	               killingButton,
				   survivalButton, 
				   densityButton, 
				   hazardButton, 
				   survivalPdeButton, 
				   conditionedDiffusionButton; 
	
	private ArrayList<Button> controls = new ArrayList<Button>();
	
	@FXML
	private MenuItem newMenuItem;
	@FXML
	private MenuItem saveAsPNGMenuItem;
	@FXML
	private MenuItem saveAsCSVMenuItem;
	@FXML
	private MenuItem wikiMenuItem;
	@FXML
	private MenuItem exitMenuItem;
	@FXML
	private ProgressBar progressBar;
	
	@FXML
	private GridPane bottomMainGrid;
	@FXML
	private Label topMainLabel, bottomMainLabel; // grid(0,0)
	@FXML
	private Button bottomMainButton1, bottomMainButton2, bottomMainButton3;
	
	// ******************** END OF FXML COMPONENTS ********************
		
	// USER INPUTS
	private UserInputs inputs;
	
	// ITO GENERATOR
	private Diffusion diffusion;
	private ConditionedDiffusion conditionedDiffusion;
	
	// ITO SOLVER
	private ItoSolver process;
	
	// FTCS SOLVER
	private FTCSSolver solver;
	
	// DIFFUSION
	private DiffusionGraph diffusionGraph;
	private DiffusionGraphPanel diffusionSidePanel;
	
	// KILLING
	private DiffusionKillingGraph diffusionKillingGraph;
	private DiffusionKillingGraphPanel diffusionKillingSidePanel;
	
	// SURVIVAL
	private DiffusionSurvivalGraph diffusionSurvivalGraph;
	private DiffusionSurvivalGraphPanel diffusionSurvivalSidePanel;
	
	// DENSITY
	private DiffusionDensityGraph diffusionDensityGraph;
	private DiffusionDensityGraphPanel diffusionDensitySidePanel;
	
	// HAZARD
	private DiffusionHazardGraph diffusionHazardGraph;
	private DiffusionHazardGraphPanel diffusionHazardSidePanel;
	
	// FTCS 
	private FTCSGraph ftcsGraph;
	private FTCSGraphPanel ftcsSidePanel;
	
	// CONDITIONED DIFFUSION
	private ConditionedDiffusionGraph conditionedDiffusionGraph;
	private ConditionedDiffusionGraphPanel conditionedDiffusionSidePanel;
	
	// FLAGS - THREE TYPES OF PROCESSES 
	private boolean isDiffusionPressed                = false;
	private boolean isConditionedDiffusionPressed     = false;
	private boolean isSurvivalPdePressed              = false;
	
	// CONDITIONED DIFFUSION FLAGS
	private boolean isConditionedDiffusionInitialized = false;
	private boolean isConditionedDiffusionDone        = false; 

	// DIFFUSION FLAGS
	private boolean isDiffusionInitialized            = false;
	private boolean isDiffusionDone                   = false;
	
	// SURVIVAL PDE FLAGS
	private boolean isSurvivalPdeInitialized          = false;
	
	// CONSTRUCTOR
	protected MainController(){
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource(Constants.FXML_RESOURCE_PATH + "MainContainer.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.getStylesheets().addAll(getClass()
				.getResource(Constants.CSS_RESOURCE_PATH + "SceneStyle.css").toExternalForm());
	}
	
	// RETURNS: SINGLETON CONTROLLER
	private static MainController instance = null;
	public static MainController getInstance() {
		if(instance == null) {
			instance = new MainController();
		}
		return instance;
	}
		
	public Button getPressedButton(){

		switch (pressedButton.getValue()) {

		case "diffusionButton" :  
			return diffusionButton;
		case "killingButton":  
			return killingButton;
		case "survivalButton":  
			return survivalButton;
		case "densityButton": 
			return densityButton;
		case "hazardButton":  
			return hazardButton;
		case "conditionedDiffusionButton":  
			return conditionedDiffusionButton;
		case "survivalPdeButton":  
			return survivalPdeButton;
		}
		return null;
	}

	
	// ******************** DIFFUSION METHODS ********************
	
	// GETS CALLED TWICE; (1) FROM THE UI BUTTON (2) FROM THE stepDiffusionProcess() METHOD 
	public void getDiffusionProcess(){
		
		// IF DIFFUSION IS NOT INITIALIZED:
		// - CREATE NEW INSTANCE
		// - WAIT FOR IT TO FINISH 
		// - SET FLAG AS INITIALIZED
		if (!isDiffusionInitialized){
			
			diffusion = new Diffusion();
			centerMainPane.getChildren().setAll(inProcess);
			rightMainPane.getChildren().clear();
			isDiffusionInitialized = true;
			
		// TODO: Needs to handle each button of diffusion
		} else if (isDiffusionDone) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {

					switch (pressedButton.getValue()) {
					
					case "diffusionButton" :  
						centerMainPane.getChildren().setAll(diffusionGraph);
						rightMainPane.getChildren().setAll(diffusionSidePanel);
						break;
					case "killingButton":  
						centerMainPane.getChildren().setAll(diffusionKillingGraph);
						rightMainPane.getChildren().setAll(diffusionKillingSidePanel);
						break;
					case "survivalButton":  
						centerMainPane.getChildren().setAll(diffusionSurvivalGraph);
						rightMainPane.getChildren().setAll(diffusionSurvivalSidePanel);
						break;
					case "densityButton": 
						centerMainPane.getChildren().setAll(diffusionDensityGraph);
						rightMainPane.getChildren().setAll(diffusionDensitySidePanel);
						break;
					case "hazardButton":  
						centerMainPane.getChildren().setAll(diffusionHazardGraph);
						rightMainPane.getChildren().setAll(diffusionHazardSidePanel);
						break;
					}
				}
			});
		}
	}
	
	// GETS CALLED FROM THE DYNAMIC DIFFUSION (OUTSIDE MAIN CONTROLLER)
	public void stepDiffusionProcess() {

		isDiffusionDone = true;

		timer.schedule(new TimerTask() {

			@Override
			public void run() {

				// Change to try
				if (diffusion != null){
					// Initiate graphs 
					
					process.setDiffusion(diffusion);
					
					diffusionGraph = new DiffusionGraph(process);
					diffusionSidePanel = new DiffusionGraphPanel(diffusionGraph);
					
					diffusionKillingGraph = new DiffusionKillingGraph(process);
					diffusionKillingSidePanel = new DiffusionKillingGraphPanel(diffusionKillingGraph);
					
					diffusionSurvivalGraph = new DiffusionSurvivalGraph(process);
					diffusionSurvivalSidePanel = new DiffusionSurvivalGraphPanel(diffusionSurvivalGraph);
					
					diffusionDensityGraph = new DiffusionDensityGraph(process);
					diffusionDensitySidePanel = new DiffusionDensityGraphPanel(diffusionDensityGraph);
					
					diffusionHazardGraph = new DiffusionHazardGraph(process);
					diffusionHazardSidePanel = new DiffusionHazardGraphPanel(diffusionHazardGraph);
					
					// This calls a method in MainCOntroller to handle the UI
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							// change this to getDiffusionProcess();
							getDiffusionProcess();
						}
					});
				} // else (optional)
			   
			}
		}, (long) 100); // End of Task
	}
	
	// ******************** CONDITIONED DIFFUSION METHODS ********************
	
	public void getConditionedDiffusionProcess(){
		
		if (!isConditionedDiffusionInitialized){
			
			// NEW CONDITIONED DIFFUSION
			conditionedDiffusion = new ConditionedDiffusion();
			centerMainPane.getChildren().setAll(inProcess);
			rightMainPane.getChildren().clear();
			
			isConditionedDiffusionInitialized = true;
			isConditionedDiffusionDone        = false;
			
		} else if (isConditionedDiffusionDone && pressedButton.getValue().equals("conditionedDiffusionButton")) {
			
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					centerMainPane.getChildren().setAll(conditionedDiffusionGraph);
					rightMainPane.getChildren().setAll(conditionedDiffusionSidePanel);
				}
			});
			
		} else if (isConditionedDiffusionDone && !pressedButton.getValue().equals("conditionedDiffusionButton")) {
			
			owner = this.getScene().getWindow();
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					// ALERT THE USER THAT CONDITIONED DIFFUSION SIMULATION IS COMPLETE
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("NOTIFICATION");
					alert.setHeaderText("CONDITIONED DIFFUSION PATH GENERATION COMPLETE");
					alert.setContentText("Would you like to open PLOT VIEW?");
					alert.initOwner(owner);
					
					final ButtonType okButtonData = new ButtonType("OK", ButtonData.YES);
					final ButtonType cancelButtonData = new ButtonType("CANCEL", ButtonData.CANCEL_CLOSE);
					
					alert.getButtonTypes().setAll(okButtonData, cancelButtonData);
					
					Optional<ButtonType> result = alert.showAndWait();
					if (result.get() == okButtonData){
						centerMainPane.getChildren().setAll(conditionedDiffusionGraph);
						rightMainPane.getChildren().setAll(conditionedDiffusionSidePanel);
					} else {
						// ... user chose CANCEL or closed the dialog
						// TODO: Maybe do nothing
					}
				}
			});
			
		} else {
			
			System.out.println("it on the else");
						
			// UPDATE UI
//			Platform.runLater(new Runnable() {
//				@Override
//				public void run() {
//					centerMainPane.getChildren().setAll(inProcess);
//				}
//			});
		}
	}
		
	// This method is called from the Dynamic Conditioned Diffusion Class.
	// When this method is called, the Conditioned Diffusion process has finished.
	// - All Data is available to be placed in the Conditioned Diffusion Graph.
	public void stepConditionedDiffusionProcess(){

		isConditionedDiffusionDone = true;
		
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				
				if (conditionedDiffusion != null){
					process.setConditionedDiffusion(conditionedDiffusion);
					conditionedDiffusionGraph = new ConditionedDiffusionGraph(process);
					conditionedDiffusionSidePanel = new ConditionedDiffusionGraphPanel(conditionedDiffusionGraph);

					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							getConditionedDiffusionProcess();
						}
					});

				} else {
					// Troubleshoot
					// At this point, it should never be null
				}
			}
		}, (long) 100); // End of Task
	}
	
	// ******************** UI METHODS ********************
	
	public void updateLabel(){
		
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				topMainLabel.setTextFill(Color.LIME);
			}
		});
		
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						topMainLabel.setTextFill(Color.TRANSPARENT);
					}
				});
			}
		}, (long) 3000); // End of Task
	}
	
	// *******************************************************
	
	public void getSurvivalPdeProcess(){
		
		if(!isSurvivalPdeInitialized) {
			solver  = new FTCSSolver();
			ftcsGraph = new FTCSGraph(solver); 
			ftcsSidePanel = new FTCSGraphPanel(ftcsGraph);
			
			isSurvivalPdeInitialized = true;
			
			// UPDATE UI
			centerMainPane.getChildren().setAll(ftcsGraph);
			rightMainPane.getChildren().setAll(ftcsSidePanel);
		} else {
			// UPDATE UI
			centerMainPane.getChildren().setAll(ftcsGraph);
			rightMainPane.getChildren().setAll(ftcsSidePanel);
		}
	}
			
	@FXML
	public void initialize()
	{	
		// SET CONTROLS
		controls.add(diffusionButton);
		controls.add(killingButton);
		controls.add(survivalButton); 
		controls.add(densityButton); 
		controls.add(hazardButton); 
		controls.add(survivalPdeButton); 
		controls.add(conditionedDiffusionButton);
		
		// SET FUNCTIONAL BUTTON STYLES
		diffusionButton.getStyleClass().add("functional-button");
		killingButton.getStyleClass().add("functional-button");
		survivalButton.getStyleClass().add("functional-button");
		densityButton.getStyleClass().add("functional-button");
		hazardButton.getStyleClass().add("functional-button");
		survivalPdeButton.getStyleClass().add("functional-button");
		conditionedDiffusionButton.getStyleClass().add("functional-button");

		// START OFF WITH NEW INPUTS
		applyNewItem();
		
		newMenuItem.setOnAction(e ->{
			
			pressedButton.set(newMenuItem.getId());
			applyNewItem();
		});
		
		wikiMenuItem.setOnAction(e -> {
			help = new Help(((Helpable)centerMainPane.getChildren().get(0)).getPageNumber());
		});
		
		saveAsPNGMenuItem.setOnAction(e ->{
			saveAsPng(((PNGable) centerMainPane.getChildren().get(0)).getChart());
		});
		
		saveAsCSVMenuItem.setOnAction(e ->{
			saveAsCsv(((CSVable) centerMainPane.getChildren().get(0)).getData());
		});
		
		exitMenuItem.setOnAction(e ->{
			terminateTasks();
			Platform.exit();
		});
		
		diffusionButton.setOnAction(e -> {
			
			pressedButton.set(diffusionButton.getId());
			getDiffusionProcess();
		});
		
		killingButton.setOnAction(e -> {
					
			pressedButton.set(killingButton.getId()); 
			getDiffusionProcess();
		});
		
		survivalButton.setOnAction(e -> {

			pressedButton.set(survivalButton.getId());
			getDiffusionProcess();
		});
		
		densityButton.setOnAction(e -> {

			pressedButton.set(densityButton.getId());
			getDiffusionProcess();
		}); 
		
		hazardButton.setOnAction(e -> {
			
			pressedButton.set(hazardButton.getId());
			getDiffusionProcess();
		}); 
		
		survivalPdeButton.setOnAction(e -> {
			
			pressedButton.set(survivalPdeButton.getId());
			getSurvivalPdeProcess();
		});
		
		conditionedDiffusionButton.setOnAction(e -> {
			
			pressedButton.set(conditionedDiffusionButton.getId());
			
			getConditionedDiffusionProcess();
		});

		// USE THIS TO UPDATE OTHER STUFF
		pressedButton.addListener(e -> {

			if (getPressedButton() != null){
				getPressedButton().getStyleClass().add("selected");
			} else {
				for(Button other : controls){
					other.getStyleClass().remove("selected");
				}
			}
			
			for(Button other : controls){
				if (other != getPressedButton()) other.getStyleClass().remove("selected");
			}
			bottomMainLabel.setText("");
			bottomMainButton1.setDisable(true);
		});
}
		
	// Needs to be wrapped with Platform.RunLater
	public void handleBottomPane(String xComponenet){

		// TODO: CHange to pressedButton
		switch (xComponenet) {
		case "NEW": 
			bottomMainLabel.setText(inputs.getSuggestion());
			bottomMainButton1.setDisable(false);
			bottomMainButton1.setText("OK");
			
			bottomMainButton1.setOnAction(e ->{
				inputs.resetDefaults();
			});
			break;
		}
	}
	
	public void applyNewItem(){
		
		// CLEAN UP ANY ON-GOING TASKS
		terminateTasks();

		process = new ItoSolver();
		inputs = new UserInputs();
		
		centerMainPane.getChildren().setAll(inputs);
		rightMainPane.getChildren().clear();
		
		// SET INITIALZED FLAGS TO FALSE FOR ALL THREE PROCESSES
		isConditionedDiffusionInitialized = false;
		isDiffusionInitialized            = false;
		isSurvivalPdeInitialized          = false;
		
		isConditionedDiffusionDone        = false;
		isDiffusionDone                   = false; 
		
		handleBottomPane("NEW");
	}
	
	// SAVE TO PNG
	public void saveAsPng(Node photo) {

	    WritableImage image = photo.snapshot(new SnapshotParameters(), null);

	    final FileChooser pngChooser = new FileChooser();
	    pngChooser.setTitle("Save Image");

	    pngChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG", "*.png")
            );
        
        File file = pngChooser.showSaveDialog(this.getScene().getWindow());
        
        if (file != null) {
        	if(!file.getName().contains(".")) {
        		  file = new File(file.getAbsolutePath() + ".png");
        		}
        	
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "PNG", file);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
	}
	
	// SAVE TO CSV 
	private void saveAsCsv(String[][] strings) {

		final FileChooser csvChooser = new FileChooser();
		csvChooser.setTitle("Save Data");

		csvChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("CSV", "*.csv")
				);

		File file = csvChooser.showSaveDialog(this.getScene().getWindow());

		if (file != null) {
			if(!file.getName().contains(".")) {
				file = new File(file.getAbsolutePath() + ".csv");
			}

			ICsvListWriter csvWriter = null;
			try {
				csvWriter = new CsvListWriter(new FileWriter(file), 
						CsvPreference.STANDARD_PREFERENCE);

				for (int j = 0; j < strings.length; j++) {
					csvWriter.write(strings[j]);
				}

			} catch (IOException e) {
				e.printStackTrace(); // TODO handle exception properly
			} finally {
				try {
					csvWriter.close();
				} catch (IOException e) {
					 // TODO handle exception properly
				}
			}
		}
	}
	
	// TERMINATES POTENTIALLY RUNNING TASKS
	public void terminateTasks(){
		if (conditionedDiffusion != null){
			conditionedDiffusion.cancelTask();
		}
	}
	
	public ProgressBar getProgressBar(){
		return this.progressBar;
	}	
}
