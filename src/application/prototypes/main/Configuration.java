package application.prototypes.main;

import org.apache.log4j.Logger;
import org.ini4j.Wini;

import java.io.*;

//http://www.javenue.info/post/40

public class Configuration {

    private static Logger LOGGER = Logger.getLogger(Configuration.class);
    		
    private static class HolderLazySingleton {
        private static Configuration instance = new Configuration();
    }

    private Wini ini = null;
    
    private Configuration() {    
    	
    	String userStringPath = System.getProperties().get("user.dir") + Constants.PARAMETERS;
    	
    	try {
    		File file = new File(userStringPath);
    		
//    		if(file.exists()){
//    			System.out.println("EXISTS: " + file.exists());
//    		} 
//    		
//    		if (file.isFile()) {
//    			System.out.println("IS FILE: " + file.isFile());
//    		} 
//    		
//    		if (file.canWrite()) {
//    			System.out.println("CAN WRITE: " + file.canWrite());
//    		}
    		
    		// PRINT THE PATH WHERE THE PARAMETERS FILE IS CREATED
    		System.out.println(file); 
    		ini = new Wini(file);
    		
    	} catch (Exception e) {
    		LOGGER.error("Exception during init of Configuration", e);
    	}
    }

    public static Configuration getInstance() {
        return HolderLazySingleton.instance;
    }

    public String getConfig(String xSectionName, String xFieldValue) {

        String readValue = null;

        if (ini.get(xSectionName, xFieldValue) != null) {
            readValue = ini.get(xSectionName, xFieldValue);
        } else {
            // TODO: What should happen
        }
        return readValue;
    }

    public void setConfig(String xSectionName, String xFieldValue, String xValue) {

        try {
            ini.put(xSectionName, xFieldValue, xValue);
            ini.store();
        } catch (Exception e1) {
            LOGGER.error(xValue + " could not be stored.", e1);         
        }
    }
}