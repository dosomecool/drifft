package application.prototypes.main;

import javafx.scene.Node;

public interface PNGable {

	public Node getChart();
}
