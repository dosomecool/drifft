package application.prototypes.main;

public class Constants {
	
	public static final String FXML_RESOURCE_PATH = "/application/prototypes/fxml/";
	
	public static final String CSS_RESOURCE_PATH = "/application/prototypes/css/";
	
	public static final String PARAMETERS = "\\Parameters.ini"; // System.getProperties().get("user.dir") 
}
