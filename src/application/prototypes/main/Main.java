package application.prototypes.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.sun.javafx.application.LauncherImpl;

import application.preloader.Splasher;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

/**
 * The Class Main.
 */
public class Main extends Application {
	
	private MainController mainPane;
	
	@Override 
	public void init() {
		
		// System.out.println(System.getProperties().get("user.dir")); 
		
		String userStringPath = System.getProperties().get("user.dir") + Constants.PARAMETERS;
		File f = new File(userStringPath);
		
		// Files.newBufferedWriter() uses UTF-8 encoding by default
		if(f.exists() && f.isFile()) {
			// TODO: If file exist ...
		} else {
			System.out.println("WRITTING NEW FILE ... ");
			
			try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(userStringPath))) 
			{	
				// DRIFT
				writer.write("[drift]");
				writer.newLine();
				writer.write("mu = 0");
				writer.newLine();
				writer.newLine();
				
				// DIFFUSION
				writer.write("[diffusion]");
				writer.newLine();
				writer.write("sigma = 1");
				writer.newLine();
				writer.newLine();
				
				// X0
				writer.write("[x0]");
				writer.newLine();
				writer.write("value = 0");
				writer.newLine();
				writer.newLine();

				// DELTA T - dt
				writer.write("[delta_time]");
				writer.newLine();
				writer.write("value = 0.01");
				writer.newLine();
				writer.newLine();
				
				// DELTA X - dx
				writer.write("[delta_state]");
				writer.newLine();
				writer.write("value = 0.01");
				writer.newLine();
				writer.newLine();
				
				writer.write("[number_paths]");
				writer.newLine();
				writer.write("value = 100");
				writer.newLine();
				writer.newLine();

				writer.write("[kill]");
				writer.newLine();
				writer.write("function = 0.25 * Math.pow(x,2)");
				writer.newLine();
				writer.newLine();
				
				writer.write("[seed]");
				writer.newLine();
				writer.write("value = 123");
				writer.newLine();
				writer.newLine();
				
				// THIS IS ONLY FOR THE FTCS
				writer.write("[spatial_intervals]");
				writer.newLine();
				writer.write("n = 32");
				writer.newLine();
				writer.newLine();

				// THIS IS ONLY FOR THE FTCS
				writer.write("[temporal_intervals]");
				writer.newLine();
				writer.write("m = 256");
				writer.newLine();
				writer.newLine();

				writer.write("[limits]");
				writer.newLine();
				writer.write("xmin = -5");
				writer.newLine();
				writer.write("xmax = 5");
				writer.newLine();
				writer.write("tmin = 0.0");
				writer.newLine();
				writer.write("tmax = 10");
				writer.newLine();
				writer.newLine();

				writer.close();
				f.canExecute();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}

		// Singleton Pattern
		mainPane = MainController.getInstance();
	}
		
	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) {
		
		try {
			Scene scene = new Scene(mainPane, 1280, 700, false, SceneAntialiasing.DISABLED); 
			scene.getStylesheets().addAll(getClass()
		    		.getResource(Constants.CSS_RESOURCE_PATH + "SceneStyle.css").toExternalForm());
			
			primaryStage.getIcons().add(new Image("icon.png"));
			primaryStage.setTitle("DRIFFT v1.0");
			
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			//.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
			primaryStage.sizeToScene();
			primaryStage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		    @Override
		    public void handle(WindowEvent event) {

		    	// ENSURE ALL RUNNING TASKS ARE TERMINATED
		    	mainPane.terminateTasks();
		    	// CONSUME  CLOSING EVENT
//		        event.consume();
		    }
		});
	}
	
	public static void main(String[] args) {
		LauncherImpl.launchApplication(Main.class, Splasher.class, args);
	}
}
