package application.prototypes.main;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JLabel;

import org.jfree.fx.FXGraphics2D;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.DialogPane;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

/**
 * The Class LatexHelper.
 */
public class LatexHelper {
	
	/** The Constant DEFAULT_PADDING_VALUE. */
	private static final int DEFAULT_PADDING_VALUE = 10;
	/** The Constant DEFAULT_LATEX_FONT_SIZE. */
	private static final int DEFAULT_LATEX_FONT_SIZE = 14;
	
	/**
	 * Gets the latex alert.
	 *
	 * @param latexString the latex string
	 * @return the latex alert
	 */
	public static Alert getLatexAlert(String latexString) {
	    GridPane grid = new GridPane();
        grid.setHgap(DEFAULT_PADDING_VALUE);
        grid.setVgap(10);
        grid.setPadding(new Insets(DEFAULT_PADDING_VALUE, DEFAULT_PADDING_VALUE, DEFAULT_PADDING_VALUE, DEFAULT_PADDING_VALUE));
        ScrollPane scrollPane = new ScrollPane();
        ImageView customImage = new ImageView(LatexHelper.getBufferedImage(latexString, 24));
        scrollPane.setContent(customImage);
        grid.add(scrollPane, 0, 0);

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Latex Output");
	    DialogPane dialogPane = alert.getDialogPane();
	    dialogPane.setHeaderText("");
	    dialogPane.setContent(grid);
	    dialogPane.setGraphic(null);
	    dialogPane.setHeader(null);
	    return alert;
	}
	
	
	/**
	 * Gets the buffered image.
	 *
	 * @param latex the latex
	 * @return the buffered image
	 */
	public static Image getBufferedImage(String latex) {
		return getBufferedImage(latex, DEFAULT_LATEX_FONT_SIZE);
	}
	
	/**
	 * Gets the buffered image.
	 *
	 * @param latex the latex
	 * @param fontSize the font size
	 * @return the buffered image
	 */
	public static Image getBufferedImage(String latex, int fontSize) {
	   
	   TeXFormula teXFormula = new TeXFormula(latex);
	   BufferedImage image = (BufferedImage) teXFormula.createBufferedImage(TeXConstants.STYLE_TEXT, fontSize, Color.BLACK, null);
	   Image fxImage = SwingFXUtils.toFXImage((BufferedImage) image, null);
	   return fxImage;
	}
	
	/**
	 * Gets the canvas.
	 *
	 * @param latex the latex
	 * @return the canvas
	 */
	public static Canvas getCanvas(String latex) {
		return new LatexCanvas(latex);
	}
	
	/**
	 * Gets the canvas.
	 *
	 * @param latex the latex
	 * @param fontSize the font size
	 * @return the canvas
	 */
	public static Canvas getCanvas(String latex, int fontSize) {
		return new LatexCanvas(latex, fontSize);
	}
	
	/**
	 * The Class LatexCanvas.
	 */
	private static class LatexCanvas extends Canvas {
	    
    	/** The g 2. */
    	private FXGraphics2D g2;

	    /** The icon. */
    	private TeXIcon icon;
    	
    	/**
	     * Instantiates a new latex canvas.
	     *
	     * @param latex the latex
	     */
	    public LatexCanvas(String latex) {
    		this(latex, DEFAULT_LATEX_FONT_SIZE);
    	}

	    /**
    	 * Instantiates a new latex canvas.
    	 *
    	 * @param latex the latex
    	 * @param fontSize the font size
    	 */
    	public LatexCanvas(String latex, int fontSize) {
	        g2 = new FXGraphics2D(getGraphicsContext2D());

	        // create a formula
	        TeXFormula formula = new TeXFormula(latex);

	        // render the formula to an icon of the same size as the formula.
	        icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, fontSize);

	        // Redraw canvas when size changes. 
	        widthProperty().addListener(evt -> draw()); 
	        heightProperty().addListener(evt -> draw()); 
	    }

	    /**
    	 * Draw.
    	 */
    	private void draw() {
	        double width = getWidth();
	        double height = getHeight();
	        getGraphicsContext2D().clearRect(0, 0, width, height);

	        // ideally it should be possible to draw directly to the FXGraphics2D
	        // instance without creating an image first...but this does not generate
	        // good output
	        // this.icon.paintIcon(new JLabel(), g2, 50, 50);

	        // now create an actual image of the rendered equation
	        BufferedImage image = new BufferedImage(icon.getIconWidth(),
	                icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
	        Graphics2D gg = image.createGraphics();
	        gg.setColor(Color.BLACK);
	        gg.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
	        
	        JLabel jl = new JLabel();
	        jl.setForeground(new Color(255, 255, 255));
	        
	        icon.paintIcon(jl, gg, 0, 0);
	        // at this point the image is created, you could also save it with ImageIO

	        g2.drawImage(image, 0, 0, null);
	    }

	    /* (non-Javadoc)
    	 * @see javafx.scene.Node#isResizable()
    	 */
    	@Override 
	    public boolean isResizable() {
	        return true;
	    }

	    /* (non-Javadoc)
    	 * @see javafx.scene.Node#prefWidth(double)
    	 */
    	@Override 
	    public double prefWidth(double height) {
    		return getWidth(); 
    	}

	    /* (non-Javadoc)
    	 * @see javafx.scene.Node#prefHeight(double)
    	 */
    	@Override 
	    public double prefHeight(double width) {
    		return getHeight(); 
    	}
	}
	
}
