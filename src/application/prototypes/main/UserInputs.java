package application.prototypes.main;

import java.io.IOException;
import java.util.Random;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class UserInputs extends Pane implements Helpable{
	
	long randSeed;
	
	@FXML
	private Pane DiffusionInputPane;
	
	// ********** DIFFUSION TEXT-FIELDS ********** 
	
	@FXML
	private TextField driftFunc;             // (0)
	@FXML
	private TextField diffusionFunc;         // (1)
	@FXML
	private TextField killingFunc;           // (2)
	@FXML
	private TextField startValueX;           // (3)
	@FXML
	private TextField startTimeT;            // (4)
	@FXML 
	private TextField finalTimeT;            // (5)
	@FXML
	private TextField deltaTimeT;            // (6)
	@FXML
	private TextField numberPathsN;          // (7)
	@FXML
	private TextField seed;                  // (8)
	
	// ********* SURVIVAL PDE TEXT-FIELDS ********* 
	
	@FXML
	private TextField boundaryXMin;          // (9)
	@FXML
	private TextField boundaryXMax;          // (10)
	@FXML
	private TextField temporalIntervalM;     // (11)
	@FXML
	private TextField spatialIntervalN;      // (12)
	
	// TODO: Maybe remove promt text completely
	// TODO: Maybe add a reset for each textfield 
	
	// ********************************************
	
	// HOLDS ALL TEXT-FIELDS
	private TextField[] controls = new TextField[13];
	
	// THIS IS THE FIRST TIME THAT _instance will be called
	private Configuration config;
	
	// TODO: This should be a lot more dynamic - and contain several 
	private String suggestion = "RESET DEFAULT VALUES?";
	
	// FXML LOADER
	public UserInputs() {
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource(Constants.FXML_RESOURCE_PATH + "ItoInput.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// SETS THE STYLE OF THIS PAGE
		DiffusionInputPane.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "UserInputStyle.css").toExternalForm());
	}
	
	@FXML
	public void initialize()
	{	
		config = Configuration.getInstance();
		
		// SEED USED FOR THE SIMULATION INSTANCE IF USER INPUT IS NULL 
		final Random random = new Random();
		randSeed = random.nextLong();
		config.setConfig("seed", "value", String.valueOf(randSeed));
		
		loadPreviousValues();
		
		controls[0]	= driftFunc;
		controls[1]	= diffusionFunc;
		controls[2]	= startValueX;
		controls[3] = deltaTimeT;
		controls[4] = killingFunc;
		controls[5]	= numberPathsN;
		controls[6] = seed;
		controls[7] = startTimeT;
		controls[8] = finalTimeT;
		
		controls[9]	= boundaryXMin;
		controls[10] = boundaryXMax;
		controls[11] = temporalIntervalM;
		controls[12] = spatialIntervalN;
		
		// TODO: NEED SOME COMPARISON TO DEFAULTS
//		loadPreviousValues();
		
		// DRIFT COEFFICIENT
		driftFunc.setOnKeyReleased(e ->{
			
			if (!driftFunc.getText().equals(""))
			{
				config.setConfig("drift", "mu", driftFunc.getText());
			} else {
				config.setConfig("drift", "mu", config.getConfig("drift", "mu"));
			}
		});
		
		// DIFFUSION COEFFICIENT
		diffusionFunc.setOnKeyReleased(e ->{
			
			if (!diffusionFunc.getText().equals(""))
			{
				config.setConfig("diffusion", "sigma", diffusionFunc.getText());
			} else {
				config.setConfig("diffusion", "sigma", config.getConfig("diffusion", "sigma"));
			}
		});

		// KILLING FUNCTION
		killingFunc.setOnKeyReleased(e ->{

			// TODO: Maybe we can Regex check Killing Function
			if (!killingFunc.getText().equals(""))
			{
				config.setConfig("kill", "function", killingFunc.getText());
			} else {
				config.setConfig("kill", "function", config.getConfig("kill", "function"));
			}
		});
				
		// START VALUE X 
		startValueX.setOnKeyReleased(e ->{
			
			// DEFAULT START VALUE X 
			double startValueXDouble = 0.0;

			if (!startValueX.getText().equals(""))
			{
				try {
					startValueXDouble = Double.parseDouble(startValueX.getText());
					config.setConfig("x0", "value", startValueX.getText());
				} catch (NumberFormatException n) {
					startValueX.getStyleClass().add("textfielderror");
				} 
			} else {
				startValueX.getStyleClass().clear();
				startValueX.getStyleClass().addAll(".text-field:focused", "text-field");
				config.setConfig("x0", "value", Double.toString(startValueXDouble));
			}
		});

		// T START VALUE
		startTimeT.setOnKeyReleased(e ->{
			
			// DEFAULT START VALUE T 
			double startValuetDouble = 0.0;

			if (!startTimeT.getText().equals(""))
			{
				try {
					startValuetDouble = Double.parseDouble(startTimeT.getText());
					config.setConfig("limits", "tmin", startTimeT.getText());
				} catch (NumberFormatException n) {
					startTimeT.getStyleClass().add("textfielderror");
				} 
			} else {
				startTimeT.getStyleClass().clear();
				startTimeT.getStyleClass().addAll(".text-field:focused", "text-field");
//				writeParameters("limits", "tmin", Double.toString(startValuetDouble));
				config.setConfig("limits", "tmin", Double.toString(startValuetDouble));
			}
		});

		// T FINAL VALUE
		finalTimeT.setOnKeyReleased(e ->{
			
			// DEFAULT FINAL VALUE T 
			double finalValuetDouble = 10.0;

			if (!finalTimeT.getText().equals(""))
			{
				try {
					finalValuetDouble = Double.parseDouble(finalTimeT.getText());
					config.setConfig("limits", "tmax", finalTimeT.getText());
				} catch (NumberFormatException n) {
					finalTimeT.getStyleClass().add("textfielderror");
				} 
			} else {
				finalTimeT.getStyleClass().clear();
				finalTimeT.getStyleClass().addAll(".text-field:focused", "text-field");
				config.setConfig("limits", "tmax", Double.toString(finalValuetDouble));
			}
		});

		// DELTA T VALUE
		deltaTimeT.setOnKeyReleased(e ->{
			
			// Default of Delta Time 
			double deltaTimeDouble = 0.01;
			
			if (!deltaTimeT.getText().equals(""))
			{
				try {
					deltaTimeDouble = Double.parseDouble(deltaTimeT.getText());
					config.setConfig("delta_time", "value", deltaTimeT.getText());
				} catch (NumberFormatException n) {
					deltaTimeT.getStyleClass().add("textfielderror");
				} 
			} else {
				deltaTimeT.getStyleClass().clear();
				deltaTimeT.getStyleClass().addAll(".text-field:focused", "text-field");
				config.setConfig("delta_time", "value", Double.toString(deltaTimeDouble));
			}
		});

		numberPathsN.setOnKeyReleased(e ->{
			
			// DEFAULT NUMBER PATHS 
			int numberPathsInt = 100;

			if (!numberPathsN.getText().equals(""))
			{
				try {
					numberPathsInt = Integer.parseInt(numberPathsN.getText());
					config.setConfig("number_paths", "value", numberPathsN.getText());
				} catch (NumberFormatException n) {
					numberPathsN.getStyleClass().add("textfielderror");
				}
			} else {
				numberPathsN.getStyleClass().clear();
				numberPathsN.getStyleClass().addAll(".text-field:focused", "text-field");
				config.setConfig("number_paths", "value", Integer.toString(numberPathsInt));
			}
		});

		
		
		// RANDOM NUMBER GENERATOR seed
		seed.setOnKeyReleased(e ->{
					
			if (!seed.getText().equals(""))
			{
				try {
					Long.parseLong(seed.getText());
					config.setConfig("seed", "value", seed.getText());
				} catch (NumberFormatException n) {
					seed.getStyleClass().add("textfielderror");
				} 
			} else {
				seed.getStyleClass().clear();
				seed.getStyleClass().addAll(".text-field:focused", "text-field");
				config.setConfig("seed", "value", String.valueOf(randSeed));
				seed.setText(config.getConfig("seed", "value"));
			}
		});
	
	// ***********************************************************************************
	
		// BOUNDARY X-MIN
		boundaryXMin.setOnKeyReleased(e ->{

			// Default of Delta Time 
			double x_min = 9999;

			if (!deltaTimeT.getText().equals(""))
			{
				try {
					x_min = Double.parseDouble(boundaryXMin.getText());
					config.setConfig("limits", "xmin", boundaryXMin.getText());
				} catch (NumberFormatException n) {
					boundaryXMin.getStyleClass().add("textfielderror");
				} 
			} else {
				boundaryXMin.getStyleClass().clear();
				boundaryXMin.getStyleClass().addAll(".text-field:focused", "text-field");
				// TODO: What to do if the value is 
				if (x_min != 9999){
					config.setConfig("limits", "xmin", Double.toString(x_min));
				} else {
					// TODO
				}
			}
		});
		
		// BOUNDARY X-MAX
		boundaryXMax.setOnKeyReleased(e ->{

			double x_max = 9999;

			if (!boundaryXMax.getText().equals(""))
			{
				try {
					x_max = Double.parseDouble(boundaryXMax.getText());
					config.setConfig("limits", "xmax", boundaryXMax.getText());
				} catch (NumberFormatException n) {
					boundaryXMax.getStyleClass().add("textfielderror");
				} 
			} else {
				boundaryXMax.getStyleClass().clear();
				boundaryXMax.getStyleClass().addAll(".text-field:focused", "text-field");
				if(x_max != 9999){
					config.setConfig("limits", "xmax", Double.toString(x_max));
				}
			}
		});
		
		// TEMPORAL INTERVALS
		temporalIntervalM.setOnKeyReleased(e ->{

			int m = 100;

			if (!temporalIntervalM.getText().equals(""))
			{
				try {
					m = Integer.parseInt(temporalIntervalM.getText());
					config.setConfig("temporal_intervals", "m", temporalIntervalM.getText());
				} catch (NumberFormatException n) {
					temporalIntervalM.getStyleClass().add("textfielderror");
				}
			} else {
				temporalIntervalM.getStyleClass().clear();
				temporalIntervalM.getStyleClass().addAll(".text-field:focused", "text-field");
				if (m != 9999){
					config.setConfig("temporal_intervals", "m", Integer.toString(m));
				}
			}
		});

		spatialIntervalN.setOnKeyReleased(e ->{

			// DEFAULT NUMBER PATHS 
			int n = 9999;

			if (!spatialIntervalN.getText().equals(""))
			{
				try {
					n = Integer.parseInt(spatialIntervalN.getText());
					config.setConfig("spatial_intervals", "n", spatialIntervalN.getText());
				} catch (NumberFormatException f) {
					spatialIntervalN.getStyleClass().add("textfielderror");
				}
			} else {
				spatialIntervalN.getStyleClass().clear();
				spatialIntervalN.getStyleClass().addAll(".text-field:focused", "text-field");
				//			writeParameters("number_paths", "value", Integer.toString(numberPathsInt));
				if (n != 9999){
					config.setConfig("spatial_intervals", "n", Integer.toString(n));
				}
			}
		});
	
	// **************************************************************************
		
}
	
	private void loadPreviousValues(){
		
		driftFunc.setText(config.getConfig("drift", "mu"));
		diffusionFunc.setText(config.getConfig("diffusion", "sigma"));
		startValueX.setText(config.getConfig("x0", "value"));
		deltaTimeT.setText(config.getConfig("delta_time", "value"));
		killingFunc.setText(config.getConfig("kill", "function"));
		numberPathsN.setText(config.getConfig("number_paths", "value"));
		seed.setText(config.getConfig("seed", "value"));
		startTimeT.setText(config.getConfig("limits", "tmin"));
		finalTimeT.setText(config.getConfig("limits", "tmax"));
		
		boundaryXMin.setText(config.getConfig("limits", "xmin"));
		boundaryXMax.setText(config.getConfig("limits", "xmax"));
		temporalIntervalM.setText(config.getConfig("temporal_intervals", "m"));
		spatialIntervalN.setText(config.getConfig("spatial_intervals", "n"));
	}
	
	// RESETS ALL FIELD DEFAULTS
	public void resetDefaults(){
		
		driftFunc.setText("0.5");
		config.setConfig("drift", "mu", "0.5");
		
		diffusionFunc.setText("1.0");
		config.setConfig("diffusion", "sigma", "1.0");
		
		startValueX.setText("0.0");
		config.setConfig("x0", "value", "0.0");
		
		deltaTimeT.setText("0.01");
		config.setConfig("delta_time", "value", "0.01");
		
		killingFunc.setText("0.25 * pow(x,2)");
		config.setConfig("kill", "function", "0.25 * pow(x,2)");
		
		numberPathsN.setText("100");
		config.setConfig("number_paths", "value", "100");
		
		seed.setText("123");
		config.setConfig("seed", "value", "123");
		
		startTimeT.setText("0.0");
		config.setConfig("limits", "tmin", "0.0");
		
		finalTimeT.setText("10.0");
		config.setConfig("limits", "tmax", "10.0");
		
		boundaryXMin.setText("-5.0");
		config.setConfig("limits", "xmin", "-5.0");
		
		boundaryXMax.setText("5.0");
		config.setConfig("limits", "xmax", "5.0");
		
		temporalIntervalM.setText("256");
		config.setConfig("temporal_intervals", "m", "256");
		
		spatialIntervalN.setText("32");
		config.setConfig("spatial_intervals", "n", "32");
	}
	
	// METHOD FOR BOTTOM MAIN PANE
	
	public String getSuggestion() {
		return suggestion;
	}

	@Override
	public int getPageNumber() {
		return 0;
	}
}
