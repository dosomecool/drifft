package application.preloader.hazard;

import java.io.IOException;

import application.prototypes.custom.labeledbutton.LabeledButton;
import application.prototypes.custom.labeledbutton.LabeledButton.Action;
import application.prototypes.main.Constants;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class DiffusionHazardGraphPanel extends AnchorPane{

	@FXML
	private AnchorPane diffusionSidePanel;
	
	private static DiffusionHazardGraph controller;

	private static Thread meanThread;
	
	// DiffusionDensityGraph
	
	public DiffusionHazardGraphPanel(DiffusionHazardGraph xController) 
	{
		controller = xController;
		
		// Constructor body
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource(Constants.FXML_RESOURCE_PATH + "diffusionGraphPanel.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		diffusionSidePanel.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "ItoGraphStyle.css").toExternalForm());
	}
			
	
	private static LabeledButton meanSwitch = new LabeledButton("DENSITY"){
		@Override
		public void plot(){
			meanSwitch.getLabel().textProperty().bind(controller.getMeanDataTask().messageProperty());
			meanThread = new Thread(controller.getMeanDataTask());
			meanThread.setDaemon(true);
			meanThread.start();
			controller.getMeanDataTask().setOnSucceeded(e ->{
				meanSwitch.setAction(Action.HIDE);
			});
		}
		@Override
		public void cancel(){
			// TODO
			
		}
		@Override
		public void show(){
			if (controller.getMeanDataTask().isDone()){
				controller.getPathMeanChart().getData().addAll(DiffusionHazardGraph.getMeanSeries());
			}
		}
		@Override
		public void hide(){
			controller.getPathMeanChart().getData().remove(0);
		}
	};
	
	@FXML
	public void initialize(){
		
		meanSwitch.getLabel().textProperty().unbind();
		meanSwitch.getLabel().setText("PENDING");
		
		meanSwitch.setAction(Action.PLOT);
		
		AnchorPane.setLeftAnchor(meanSwitch, (double) 25);
		
		diffusionSidePanel.getChildren().add(meanSwitch);
		meanSwitch.setLayoutY(30);
	}
}