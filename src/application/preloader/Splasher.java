package application.preloader;

import java.io.IOException;

import application.prototypes.main.Constants;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.application.Preloader;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public final class Splasher extends Preloader {	

    Stage stage;
    Scene scene;
    
    @FXML
    private Pane splashPane;
    @FXML
    private Label splashTitleLabel, splashLoadLabel;
    @FXML
    private ImageView logoView;
 
    Timeline timeline = new Timeline(new KeyFrame(Duration.millis(200), 
    		ae -> setProgress()
    		));
    
    int indText = 0;	
    String initText;
    
    public void setProgress(){
    	
    	if (indText == 5){
    		Platform.runLater(new Runnable() {
        		public void run() {
        			splashLoadLabel.setText(initText);
        		}
        	});
    		indText = 0;
    	}

    	Platform.runLater(new Runnable() {
    		public void run() {
    			splashLoadLabel.setText(splashLoadLabel.getText() + ".");
    		}
    	});
    	indText = indText + 1;
    }
    
	public Splasher(){
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource(Constants.FXML_RESOURCE_PATH + "Splash.fxml"));
			loader.setController(this);
			loader.setRoot(splashPane);
			loader.load();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		splashPane.getStylesheets().addAll(getClass()
				.getResource(Constants.CSS_RESOURCE_PATH + "SplashStyle.css").toExternalForm());
	}
    
    @FXML
    public void initialize(){
    	
    	logoView.setImage(new Image("icon.png"));
    	initText = splashLoadLabel.getText();
    	
    	final Text t = new Text();
    	t.setX(118.0f);
    	t.setY(285.0f);
    	t.setCache(true);
    	t.setText("DRIFFT");
    	
    	t.setId("title-text");
    	
    	t.setFont(Font.font(null, FontWeight.BOLD, 110));
    	 
//    	final Reflection r = new Reflection();
//    	r.setFraction(0.75f);
//    	 
//    	t.setEffect(r);
//    	t.setTranslateY(150);
        
        splashPane.getChildren().add(t);
        
        splashLoadLabel.setEllipsisString(">");
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }
	
    private Scene createPreloaderScene() {
    	
        scene = new Scene(splashPane, splashPane.getPrefWidth(), splashPane.getPrefHeight());
        scene.setFill(Color.TRANSPARENT);
        return scene;
    }
 
    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        stage.setScene(createPreloaderScene());
        stage.getIcons().add(new Image("icon.png"));
		stage.initStyle(StageStyle.TRANSPARENT);
        stage.show();
    }
 
    @Override
    public void handleStateChangeNotification(StateChangeNotification evt) {

    	if (evt.getType() == StateChangeNotification.Type.BEFORE_START){
    		timeline.stop();
    		stage.hide();
    	}
    }
 }