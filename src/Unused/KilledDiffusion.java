package application.prototypes;

import java.util.Random;

// DOES NOT WORK YET

public class KilledDiffusion {

	// ITO DIFFUSION AND PROPERTIES
	private ItoDiffusion ito;
	
	private double x0;
	private double mu;
	private double sigma;
	private int numPath;
	
	private double h; // Delta(t) 
	
	private int mIto; // Temporal interval i.e. 
	
	// FTCS AND PROPERTIES
	private FTCS pde;

	private int m; 
	private int n;
	private double k; // Delta(x)
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	// KILLED DIFFUSION
	private double[][] xStar;
	private Random dw;
	private double[][] dxSurvival;
	
	public KilledDiffusion(ItoDiffusion xItoDiffusion) {
		
		System.out.println("KILLED CONSTRUCTOR");
		
		if (xItoDiffusion != null){
			System.out.println("ITO DIFFUSION IS NOT NULL");
			this.ito = xItoDiffusion;
		} else {
			System.out.println("ITO DIFFUSION IS NULL");
		}
		
		this.pde = new FTCS(ito.getLambda(), 
	            			ito.getMu(), 
	            			ito.getSigma(), 
	            			ito.getxMin(), 
	            			ito.getxMax(), 
	            			ito.getTmin(), 
	            			ito.getTmax()
	            			);
		
		//FTCS implementation
		pde.setDelta();
		pde.populateIndex();
		pde.initializeSolutionArray();
		pde.setInitialCondition();
		pde.setBoundaryCondition();
		pde.setKilling();
		pde.solve();
		pde.setPoints();
		
		// ITO DIFFUSION
		this.h = ito.getH();			// Delta time
		this.mIto = ito.getM();
		
		this.x0 = ito.getX0();
		this.mu = ito.getMu();
		this.sigma = ito.getSigma();
		this.numPath = ito.getNumPath();
		
		// FTCS	
		this.m = pde.getM();            // Time index		
		this.n = pde.getN();			// Space index
		this.k = pde.getK();			// Delta space
		
		this.xStar = new double[numPath][mIto+1];
		this.dw = new Random();
		
		this.dxSurvival = new double[n+1][m+1];
		
		getChangeInSurvivalPDE();
		generateKilledPath();
	}
    
	public void getChangeInSurvivalPDE(){
		
		//solution = new double[n+1][m+1];
		// m = temporal
		// n = spatial
		
		for(int j=0; j<=m; j++)
		{
			for(int i=0; i<=n-1; i++) 
			{	
				dxSurvival[i][j] = ( pde.getSolution()[i+1][j] - pde.getSolution()[i][j]) / k;
				System.out.println("sol[i+1]: " + pde.getSolution()[i+1][j] + " - " + pde.getSolution()[i][j]);
			}
		}
	}
	
	public void generateKilledPath() 
	{
		for(int index = 0; index < numPath; index++)
		{
			xStar[index][0] = x0;
			
			for(int j=0; j<=mIto-1; j++) 
			{
				// getDxGivenT needs the time of the process and the | point evaluated x
				// time needs to be converted in FTCS class // j * h
				// index needs to be converted in FTCS 
				
				xStar[index][j+1] = xStar[index][j] + 
						
						( mu + sigma * getFBarInterpolatedDerivative((j * h), xStar[index][j]) ) * h 
						+ sigma * Math.sqrt(h)*dw.nextGaussian();
			}
		}
	}
	
	public double getFBarInterpolatedDerivative(double t, double x) {

		double x1 = 0;
		double x2 = 0;
		int x1Index = 0; 
    	int x2Index = 0;
    	
    	double t1 = 0;
		double t2 = 0;
    	int t1Index = 0;
    	int t2Index = 0;
    	
    	double Q11;
    	double Q12;
    	double Q21;
    	double Q22;
		
		for(int i=0; i<pde.getX().length-1; i++)
		{
			if ( pde.getX()[i] <= x && x <= pde.getX()[i+1])
    		{
				x1 = pde.getX()[i];
    			x2 = pde.getX()[i+1];
    			x1Index = i;
            	x2Index = (i+1);
    		}
		}
		
		for(int j=0; j<(pde.getT().length-1); j++)
    	{
    		if ( pde.getT()[j] <= t && t <= pde.getT()[j+1])
    		{
    			t1 = pde.getT()[j];
    			t2 = pde.getT()[j+1];
    			t1Index = j;
            	t2Index = (j+1);
    		}
    	}
		
		Q11 = pde.getPoints()[x1Index][t1Index];
    	Q12 = pde.getPoints()[x1Index][t2Index];
    	
    	Q21 = pde.getPoints()[x2Index][t1Index];
    	Q22 = pde.getPoints()[x2Index][t2Index];
    	
    	double QX1, QX2;
    	
    	double functionValue;
    	double functionDerivative;
    	double logDerivative;
    	
    	QX1 = Q11 + (t - t1) * ((Q12 - Q11) / (t2 - t1));
    	
    	QX2 = Q21 + (t - t1) * ((Q22 - Q21) / (t2 - t1));
    	
    	functionValue = Q11 + (x - x1) * ((Q12 - Q11) / (x2 - x1));
    	
    	functionDerivative = (QX1 - QX2) / 2*h;
    	
    	logDerivative = functionValue / functionDerivative; 
    	
		return logDerivative;
	}
	
	public double[][] getxStar() {
		return xStar;
	}
	
	public int getNumPath() {
		return numPath;
	}
	
	public int getM() {
		return mIto;
	}
	
	public double[] getT(){
		return ito.getT();
	}
	
	public double getTmin(){
		return ito.getTmin();
	}
	
	public double getTmax(){
		return ito.getTmax();
	}
	
	public double getXMin(){
		return ito.getxMin();
	}
	
	public double getXMax(){
		return ito.getxMax();
	}
		
}
