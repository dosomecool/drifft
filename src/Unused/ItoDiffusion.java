package application.prototypes;

import java.util.Random;

public class ItoDiffusion {
	
	private double mu;
	private double sigma;
	private double lambda;
	private double x0;

	// Range of Time (t);
	private double tmin;
	private double tmax;

	// Number of Temporal intervals;
	private int m;
	
	// Delta (t)
	private double h;
	
	// Number of Paths
	private int numPath;
	
	private double[] t;
	
	// Stores the killing function
	private double[][] kill;

	// X 
	private double[][] x;
	private double[] meanX;
	private double[] varianceX;
	
	// X-STAR
	private double[][] xStar;
	private double[] meanxStar;
	private double[] variancexStar;

	// SURVIVAL
	private double[][] itoSurvival;
	private double[] meanSurvival;
	private double[] varianceSurvival;
	
	// DENSITY
	private double[] itoDensity;
	
	ItoDiffusion(UserInputs xInputs) 
	{
		// Constructor body
		
		// Drift
		if (xInputs.getDriftCoefValue().isNaN()){
			mu=0.0;
		} else {
			mu = xInputs.getDriftCoefValue();
		}
	
		// Diffusion
		if (xInputs.getDiffusionCoefValue().isNaN()){
			sigma=1;
		} else {
			sigma = xInputs.getDiffusionCoefValue();
		}
		
		// Lambda
		if (xInputs.getKillingCoefValue().isNaN()){
			lambda=0.25;
		} else {
			sigma = xInputs.getKillingCoefValue();
		}
		
		// X(0)
		if (xInputs.getStartValueXValue().isNaN()){
			x0=0;
		} else {
			x0 = xInputs.getStartValueXValue();
		}

		// MINIMUM TIME
		if (xInputs.getStartTimeTValue().isNaN()){
			tmin=0;
		} else {
			tmin = xInputs.getStartTimeTValue();
		}

		// MAXIMUM TIME
		if (xInputs.getFinalTimeTValue().isNaN()){
			tmax=10;
		} else {
			tmax = xInputs.getFinalTimeTValue();
		}
		
		// DELTA TIME
		if (xInputs.getDeltaTimeTValue().isNaN()){
			h = 0.01;
			m = (int) (( tmax-tmin ) / h);
		} else {
			h = xInputs.getDeltaTimeTValue();
			m = (int) (( tmax-tmin ) / h);
		}
		
		// NUMBER OF PATHS
		if (xInputs.getNumberPathNValue().isNaN()){
			numPath=10;
		} else {
			numPath = xInputs.getNumberPathNValue().intValue();
		}
		
		// Stores index of Time (t)
		t = new double[m+1];
		
		// Stores the killing function
		kill = new double[numPath][m+1];
		
		// X
		x = new double[numPath][m+1];
		meanX = new double[m+1];
		varianceX = new double[m+1];
		
	    // X-STAR
		xStar = new double [numPath][m+1];
		meanxStar = new double[m+1];
		variancexStar = new double[m+1];
		
		// SURVIVAL
		itoSurvival = new double[numPath][m+1];
		meanSurvival = new double[m+1];
		varianceSurvival = new double[m+1];
		
		// DENSITY
		itoDensity = new double[m+1];
		
		runSimulation();
	}
	
	public void populateIndex()
	{
		for(int j=0; j<=m; j++) 
		{
			t[j] = tmin + j * h;
		}
	}

	Random dw = new Random();
	
	private double xMax = 0;
	private double xMin = 0;
	
	public void generatePath() 
	{
		for(int index = 0; index < numPath; index++)
		{
			x[index][0] = x0;
			
			for(int j=0; j<=m-1; j++) 
			{
				x[index][j+1] = x[index][j] + mu * h + sigma * Math.sqrt(h)*dw.nextGaussian();
				
				// Stores the highest x value
				if (x[index][j+1] > xMax){
					xMax = x[index][j+1];
				}
				
				// Stores the lowest x value
				if (x[index][j+1] < xMin){
					xMin = x[index][j+1];
				}
			}
		}
	}
	
	public void meanX()
	{	
		for(int j=0; j<=m; j++)
		{
			double meanSum = 0;
			
			for(int index = 0; index < numPath; index++ )
			{
				meanSum = meanSum + x[index][j];
			}
			meanX[j] = meanSum / numPath;
		}
	}
	
	public void varianceX()
	{	
		for(int j=0; j<=m; j++)
		{
			double varianceSum = 0;
			
			for(int index = 0; index < numPath; index++ )
			{
				varianceSum = varianceSum + 
						(( x[index][j] - meanX[j] ) * ( x[index][j] - meanX[j] ));
			}
			
			varianceX[j] = Math.sqrt(varianceSum / numPath);
		}
	}
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	private double xStarMax = 0;
	private double xStarMin = 0;
	
	public void generateStarPath() 
	{
		for(int index = 0; index < numPath; index++)
		{
			xStar[index][0] = x0;
			
			for(int j=0; j<=m-1; j++) 
			{
				xStar[index][j+1] = xStar[index][j] 
						
								  + (mu - sigma * 2 * lambda * xStar[index][j]) * h 
								  
								  + sigma * Math.sqrt(h)*dw.nextGaussian();
				
				// Stores the highest x value
				if (xStar[index][j+1] > xStarMax){
					xStarMax = xStar[index][j+1];
				}
				
				// Stores the lowest x value
				if (xStar[index][j+1] < xStarMin){
					xStarMin = xStar[index][j+1];
				}
			}
		}
	}
	
	public void meanxStar()
	{	
		for(int j=0; j<=m; j++)
		{
			double meanSum = 0;
			
			for(int index = 0; index < numPath; index++ )
			{
				meanSum = meanSum + xStar[index][j];
			}
			meanxStar[j] = meanSum / numPath;
		}
	}
	
	public void variancexStar()
	{	
		for(int j=0; j<=m; j++)
		{
			double varianceSum = 0;
			
			for(int index = 0; index < numPath; index++ )
			{
				varianceSum = varianceSum + 
						(( xStar[index][j] - meanxStar[j] ) * ( xStar[index][j] - meanxStar[j] ));
			}
			
			variancexStar[j] = Math.sqrt(varianceSum / numPath);
		}
	}
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	public void setKilling()
	{
		for(int index = 0; index < numPath; index++ )
		{
			for(int i=0; i<=m; i++)
			{
				kill[index][i] = lambda * x[index][i] * x[index][i];
			}
		}
	}
	
	public void itoSurvival() 
	{
		for(int index = 0; index < numPath; index++ )
		{
			itoSurvival[index][0]=1;
					
			for(int j=0; j<=m-1; j++)
			{
				itoSurvival[index][j+1] = (1 - kill[index][j] * h) 
						* itoSurvival[index][j];
			}
		}
	}
	
	public void meanSurvival()
	{
		for(int j=0; j<=m; j++)
		{
			double meanSum = 0;
		
			for(int index = 0; index < numPath; index++)
			{
				meanSum = meanSum + itoSurvival[index][j];
			}
			meanSurvival[j] = meanSum / numPath;
		}
	}
	
	public void varianceSurvival()
	{
		for(int j=0; j<=m; j++)
		{
			double varianceSum = 0;
			
			for(int index = 0; index < numPath; index++ )
			{
				varianceSum = varianceSum + 
						(( itoSurvival[index][j] - meanSurvival[j] ) * 
								( itoSurvival[index][j] - meanSurvival[j] ));
			}
			varianceSurvival[j] = Math.sqrt(varianceSum / numPath);
		}
	}
	
	private double densityMax = 0;
	
	public void itoDensity() 
	{
		itoDensity[0] = 0;
		
		for(int j=1; j<=m-1; j++)
		{
			itoDensity[j+1] = ( meanSurvival[j-1] - meanSurvival[j+1]) / (2*h);
			
			// Stores the highest x value
			if (itoDensity[j+1] > densityMax){
				densityMax = itoDensity[j+1];
			}
		}
	}
	
	double[] itoCumulativeDist;
	public void itoCumulativeDistribution() 
	{
		itoCumulativeDist = new double[m+1];
		itoCumulativeDist[0] = 0;

		for(int j=0; j<=m-1; j++)
		{
			itoCumulativeDist[j+1] = itoCumulativeDist[j] + itoDensity[j+1] * h;
		}
	}
	
	// Runs all methods 
	public void runSimulation()
	{
        populateIndex();
        
        generatePath();
        meanX();
        varianceX();
        
        generateStarPath();
        meanxStar();
        variancexStar();
        
        setKilling();
        
        itoSurvival();
        meanSurvival();
        varianceSurvival();
        
        itoDensity();
        itoCumulativeDistribution();
	}
	
	// Getters
	public double[][] getItoSurvival() {
		return itoSurvival;
	}
	
	public int getM() {
		return m;
	}
	
	public double getX0(){
		return x0;
	}
	
	public double getLambda(){
		return lambda;
	}
	
	public double getMu(){
		return mu;
	}
	
	public double getSigma(){
		return sigma;
	}
	
	public int getNumPath() {
		return numPath;
	}
	
	public double[] getT() {
		return t;
	}
	
	public double getH() {
		return h;
	}
	
	public double getTmin() {
		return tmin;
	}
	
	public double getTmax() {
		return tmax;
	}
	
	public double[][] getX() {
		return x;
	}
	
	public double getxMin() {
		return xMin;
	}
	
	public double getxMax() {
		return xMax;
	}
	
	public double[][] getxStar() {
		return xStar;
	}
	
	public double getxStarMin() {
		return xStarMin;
	}
	
	public double getxStarMax() {
		return xStarMax;
	}
	
	public double[] getMeanX() {
		return meanX;
	}
	
	public double[] getMeanSurvival() {
		return meanSurvival;
	}
	
	public double[] getVarianceSurvival() {
		return varianceSurvival;
	}

	public double[] getVarianceX() {
		return varianceX;
	}
	
	public double[] getItoDensity() {
		return itoDensity;
	}
	
	public double getDensityMax() {
		return densityMax;
	}
	
	public double[] getItoCumulativeDist() {
		return itoCumulativeDist;
	}
	
	public double getItoLogCumulativeDist(int jIindex) {
		return Math.log(itoCumulativeDist[jIindex]);
	}
	
	public double[] getMeanxStar() {
		return meanxStar;
	}
	
	public double[] getVariancexStar() {
		return variancexStar;
	}
}
