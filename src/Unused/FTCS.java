package application.prototypes;

////FTCS implementation
//FTCS test = new FTCS();
//test.setDelta();
//test.populateIndex();
//test.initializeSolutionArray();
//test.setInitialCondition();
//test.setBoundaryCondition();
//test.setKilling();
//test.solve();
//test.setPoints();

public class FTCS {
	
	private double lam;
	private double mu;
	private double sig;  
	
	// Range of Space (x);
	private double xmin;
	private double xmax;

	// Range of Time (t);
	private double tmin;
	private double tmax;
	
	// Number of Spatial intervals;
	private int n=32;
	
	// Number of Temporal intervals;
	private int m=128;
	
	// Delta (x)
	private double k;
	
	//Delta (t)
	private double h;
	
	// Stores index of Space (x)
	private double[] x;
	
	// Stores index of Time (t)
	private double[] t;
	
	// Stores the killing function
	private double[] kill;
	
	// Stores the solution
	private double[][] solution;
	
	private double[][] points;
	
	FTCS(double xlam, double xMu, double xSig, double xXmin, double xXmax, double xTmin, double xTmax)
	{
		// Variables
		this.lam = xlam;
		this.mu =  xMu;
		this.sig = xSig;  

		// Range of Space (x);
		this.xmin = xXmin;
		this.xmax = xXmax;

		// Range of Time (t);
		this.tmin = xTmin;
		this.tmax = xTmax;
		
		// Number of Spatial intervals;
		this.n = 32; //128;
		
		// Number of Temporal intervals;
		this.m = 128; //4096;
		
		x = new double[n+1];
		
		t = new double[m+1];
		
		kill = new double[n+1];
		
		solution = new double[n+1][m+1];
		
		points = new double[n+1][m+1];
		
		setDelta();
		populateIndex();
		initializeSolutionArray();
		setInitialCondition();
		setBoundaryCondition();
		setKilling();
		solve();
		setPoints();
	}
	
	
	// Calculates delta x and delta t
	public void setDelta()
	{
		// Delta x;
		k = ( xmax-xmin )/n;

		// Delta t;
		h = ( tmax-tmin )/m;
	}
	
	// Populate index of (x) and (t)
	public void populateIndex()
	{
		for(int i=0; i<=n; i++) 
		{
			x[i] = xmin + i * k;
		}
		
		for(int j=0; j<=m; j++) 
		{
			t[j] = tmin + j * h;
		}
	}
	
	public void initializeSolutionArray()
	{
		for(int j=0; j<=m; j++)
		{
			for(int i=0; i<=n; i++)
			{
				solution[i][j]=0;
			}
		}
	}
	
	// Set initial condition FBAR[x,0]=1;
	public void setInitialCondition()
	{
		for(int i=0; i<=n; i++)
		{
			solution[i][0]=1;
		}
	}
	
	// Set boundary condition
	public void setBoundaryCondition()
	{
		for(int j=0; j<=m-1; j++)
		{
			solution[0][j+1]=0;
			solution[n][j+1]=0;
		}
	}
	
	public void setKilling()
	{
		for(int i=0; i<=n; i++)
		{
			kill[i] = lam * x[i] * x[i];
		}
	}
	
	// Solve
	public void solve()
	{	
		for(int j=0; j<=m-1; j++)
		{
			for(int i=0; i<=n-2; i++)
			{
				solution[i+1][j+1]=
						// Term (i-1)
						( (- mu * h) / (2 * k) + ( ((sig * sig)/2) * h ) / (k * k) ) * solution[i][j] 
						
						// Term (i)
						+ (1-(kill[i+1] * h)+ ( (-2 * ((sig * sig)/2) * h) / (k * k) )) * solution[i+1][j] 
						
						// Term (i+1)
						+ ( (mu * h) / (2 * k) + ( ((sig * sig)/2) * h ) / (k * k) ) * solution[i+2][j];
			}
		}
	}
		
	
	
	public void setPoints()
	{
		for(int j=0; j<=m; j++)
		{
			for(int i=0; i<=n-1; i++)
			{
				points[i][j] = Math.round(solution[i][j]*1000d)/1000d;
			}
		}
	}
   
	public void setLam(double lam) {
		this.lam = lam;
	}
	
	public void setMu(double mu) {
		this.mu = mu;
	}
	
	public void setSig(double sig) {
		this.sig = sig;
	}
	
	public double getXmin() {
		return xmin;
	}
	
	public double getXmax() {
		return tmax;
	}
	
	public double getTmin() {
		return tmin;
	}
	
	public double getTmax() {
		return tmax;
	}
	
	public void setN(int n) {
		this.n = n;
	}
	
	public int getN(){
		return n;
	}
	
	public void setM(int m) {
		this.m = m;
	}
	
	public int getM(){
		return m;
	}
	
	public double getH(){
		return h;
	}
	
	public double getK(){
		return k;
	}
	
	public double[] getX() {
		return x;
	}
	
	public double[] getT() {
		return t;
	}
	
	public double[][] getSolution() {
		return solution;
	}
	
	public double[][] getPoints(){
		return points;
	}
}
	
