# DRIFFT 

This program is a stochastic-differential-equation simulation software that focuses on survival analysis (time-to-event). 

## Built With

* [JavaFX](http://docs.oracle.com/javase/8/javase-clienttechnologies.htm)

## Authors

* **Edvin Beqari | edbeqari@vt.edu** 

### Prerequisites

Option 1 (Download and Run) - "Drifft.zip" includes its own JRE and it is ready to go.  

Option 2 (Light) - "Drifft-1.0.jar" requires a Java Runtime Environment (JRE) 

### Installing

Option 1 (Download and Run)

1. Download the file named "Drifft.zip" to your local computer. 
2. Unzip the content to an accessible location i.e.: Desktop.
3. Double-click on the file named "Drifft.exe."

Option 2 (Light)

1. (If necessary) Download and install the latest JRE package.
2. Download the file named "Drifft-1.0.jar" to your local computer. 
3. Double-click to run it. 

To run from the Command Line
1. Open Command Line (CMD)
2. Navigate to the folder where "Drifft-1.0.jar" is located.
3. Type "java -jar "Drifft-1.0.jar"

Example:
```
C:\Users\BQ\Downloads\java -jar Drifft-1.0.jar
```

Note: If the program does not start immediately - then the java folder must be added to the system path. 

## Getting Started

1. Double-click on the program
2. Enter the model parameters 
3. Select any of the following functionalities

    * Diffusion
    * Killing
    * Survival
    * Density 
    * Hazard
    * Conditioned Diffusion
    * Survival PDE